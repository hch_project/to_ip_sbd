/**
 * 
 */
package com.hch.equimed.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.hch.equimed.model.Sprzet;

/**
 * @author Magic
 *
 */
public class Test_Sprzet extends Sprzet{

	/**
	 * Test method for {@link com.hch.equimed.model.Sprzet#setNr_ID(long)}.
	 */
	@Test
	public void testSetNr_ID() {
		setNr_ID(1111);
		assertEquals(1111, getNr_ID());
	}

	/**
	 * Test method for {@link com.hch.equimed.model.Sprzet#setProducent(java.lang.String)}.
	 */
	@Test
	public void testSetProducent() {
		setProducent("SECA");
		assertEquals("SECA", getProducent());
	}

	/**
	 * Test method for {@link com.hch.equimed.model.Sprzet#setDataZakupu(java.lang.String)}.
	 */
	@Test
	public void testSetDataZakupu() {
		setDataZakupu("12.12.2012");
		assertEquals("12.12.2012", getDataZakupu());
	}

	/**
	 * Test method for {@link com.hch.equimed.model.Sprzet#setDw(java.lang.String)}.
	 */
	@Test
	public void testSetDw() {
		setDw("15.12.2015");
		assertEquals("15.12.2015", getDw());
	}

	/**
	 * Test method for {@link com.hch.equimed.model.Sprzet#setGwarancja(java.lang.String)}.
	 */
	@Test
	public void testSetGwarancja() {
		setGwarancja("12.12.2018");
		assertEquals("12.12.2018", getGwarancja());
	}

	/**
	 * Test method for {@link com.hch.equimed.model.Sprzet#setOstatniPrzeglad(java.lang.String)}.
	 */
	@Test
	public void testSetOstatniPrzeglad() {
		setOstatniPrzeglad("12.10.2015");
		assertEquals("12.10.2015", getOstatniPrzeglad());
	}

	/**
	 * Test method for {@link com.hch.equimed.model.Sprzet#setID_oddzialu(int)}.
	 */
	@Test
	public void testSetID_oddzialu() {
		setID_oddzialu(101);
		assertEquals(101, getID_oddzialu());
	}

	/**
	 * Test method for {@link com.hch.equimed.model.Sprzet#setNazwa(java.lang.String)}.
	 */
	@Test
	public void testSetNazwa() {
		setNazwa("EKG");
		assertEquals("EKG", getNazwa());
	}

	/**
	 * Test method for {@link com.hch.equimed.model.Sprzet#setNr_in(int)}.
	 */
	@Test
	public void testSetNr_in() {
		setNr_in(12314);
		assertEquals(12314, getNr_in());
	}

}
