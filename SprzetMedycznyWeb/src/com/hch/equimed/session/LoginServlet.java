package com.hch.equimed.session;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.hch.equimed.dao.UserDAOImpl;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// get request parameters for userID and password
		String user = request.getParameter("user");
		String pwd = request.getParameter("pwd");

		if (UserDAOImpl.checkUser(user, pwd)) {
			HttpSession session = request.getSession();
			session.setAttribute("user", "" + user + "");
			// setting session to expiry in 30 mins
			session.setMaxInactiveInterval(30 * 60);
			Cookie userName = new Cookie("user", user);
			// setting cookie to expiry in 30 mins
			userName.setMaxAge(30 * 60);
			response.addCookie(userName);
			response.sendRedirect("http://localhost:8080/SprzetMedycznyWeb/user?action=logOn");
		} else {
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/");
			PrintWriter out = response.getWriter();
			out.println("<body onload=\"window.alert('Niepoprawne dane logowania!')\"></body>");
			rd.include(request, response);
		}
	}
}
