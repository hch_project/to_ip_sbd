package com.hch.equimed.controller;

import com.hch.equimed.dao.*;
import com.hch.equimed.model.*;

import java.io.IOException;
import java.util.Formatter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class EquimedController extends HttpServlet {
	private static EquimedDAO equimedDao = new EquimedDAOImpl();
	private static EditDAO editDao = new EditDAOImpl();
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EquimedController() {
		super();
	}

	public void init(ServletConfig config) throws ServletException {
		super.init(config);

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String base = "/jsp/";
		// String url = "login.html";
		String url = "logIn.jsp";
		String action = request.getParameter("action");
		String keyWord = request.getParameter("keyWord");
		String id_oddzialu = request.getParameter("id_oddzialu");
		String id_wyp = request.getParameter("id_wyp");
		String id_oddzialu_w = request.getParameter("id_oddzialu_w");
		String id_oddzialu_p = request.getParameter("id_oddzialu_p");
		String data_p = request.getParameter("data_p");
		String data_k = request.getParameter("data_k");
		String nr_id = request.getParameter("nr_id");
		String nazwa = request.getParameter("nazwa");
		String producent = request.getParameter("producent");
		String nr_in = request.getParameter("nr_in");
		String data_zakupu = request.getParameter("data_zakupu");
		String dw = request.getParameter("dw");
		String gwarancja = request.getParameter("gwarancja");
		String ostatni_przeglad = request.getParameter("ostatni_przeglad");
		String nr_sr = request.getParameter("nr_sr");
		String opis = request.getParameter("opis");
		String data = request.getParameter("data");

		if (action != null) {
			switch (action) {
			case "main":
				findAllEquipment(request, response);
				findAllBranch(request, response);
				url = base + "home.jsp";
				break;
			case "allEquip":
				findAllEquipment(request, response);
				findAllBranch(request, response);
				url = base + "listOfEquip.jsp";
				break;
			case "search":
				searchEquipmentByKeyword(request, response, keyWord);
				findAllBranch(request, response);
				url = base + "searchResult.jsp";
				break;
			case "moreInfo":
				findAllEquipment(request, response);
				findAllBranch(request, response);
				findLoanEquipment(request, response);
				url = base + "equipInfo.jsp";
				break;

			// funkcje dla LEKARZA
			case "allEquipLP":
				findAllEquipment(request, response);
				findAllBranch(request, response);
				url = base + "listOfEquip_lp.jsp";
				break;
			case "loanIn":
				findAllBranch(request, response);
				findAllEquipment(request, response);
				url = base + "loan.jsp";
				break;
			case "loan":
				equimedDao.saveLoan(Integer.parseInt(id_wyp), Integer.parseInt(id_oddzialu_w),
						Integer.parseInt(id_oddzialu_p), data_p, data_k, Integer.parseInt(nr_id));
				editDao.changeStatus(Integer.parseInt(nr_id), 0, 1);
				findAllEquipment(request, response);
				findAllBranch(request, response);
				url = base + "listOfEquip_lp.jsp";
				break;
			case "loanEquip": // wyświetlenie listy pożyczonego sprzetu
				findLoanEquipment(request, response);
				findAllBranch(request, response);
				url = base + "loanEquip.jsp";
				break;
			case "deleteLoan":
				equimedDao.deleteLoanEquipment(Integer.parseInt(nr_id));
				findLoanEquipment(request, response);
				findAllBranch(request, response);
				url = base + "loanEquip.jsp";
				break;

			// funkcje dla SERWISU
			case "mainSerwis":
				findAllEquipment(request, response);
				findAllBranch(request, response);
				url = base + "home_serwis.jsp";
				break;
			case "allEquipSerwis":
				findAllEquipment(request, response);
				findAllBranch(request, response);
				url = base + "listOfEquip_serwis.jsp";
				break;
			case "repair":
				findAllEquipment(request, response);
				findAllBranch(request, response);
				url = base + "repair.jsp";
				break;
			case "addRepair":
				equimedDao.saveRepair(Integer.parseInt(nr_sr), opis, data, Integer.parseInt(nr_id));
				findAllEquipment(request, response);
				findAllBranch(request, response);
				url = base + "listOfEquip_serwis.jsp";
				break;

			// funkcje dla inwentaryzacji
			case "allEquipInw":
				findAllEquipment(request, response);
				findAllBranch(request, response);
				url = base + "listOfEquip_inw.jsp";
				break;
			case "addEquipPage":
				findAllEquipment(request, response);
				nextID(request, response);
				findAllBranch(request, response);
				url = base + "addEquip.jsp";
				break;
			case "addEquip":
				equimedDao.addEquip(Integer.parseInt(nr_id), nazwa, producent, Integer.parseInt(nr_in), data_zakupu, dw,
						gwarancja, ostatni_przeglad, Integer.parseInt(id_oddzialu));
				findAllEquipment(request, response);
				findAllBranch(request, response);
				url = base + "listOfEquip_inw.jsp";
				break;
			}
		}
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(url);
		requestDispatcher.forward(request, response);
	}

	private void findAllBranch(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			EquimedDAO equimedDao = new EquimedDAOImpl();
			List<Oddzial> branchList = equimedDao.findAllBranch();
			request.setAttribute("branchList", branchList);

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	private void findAllEquipment(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			EquimedDAO equimedDao = new EquimedDAOImpl();
			List<Sprzet> equipmentList = equimedDao.findAllEquipment();
			request.setAttribute("equipmentList", equipmentList);

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	private void searchEquipmentByKeyword(HttpServletRequest request, HttpServletResponse response, String keyWord)
			throws ServletException, IOException {
		try {
			EquimedDAO equimedDao = new EquimedDAOImpl();
			List<Sprzet> brandList = equimedDao.searchEquipmentByKeyword(keyWord);

			request.setAttribute("brandList", brandList);

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	private void findLoanEquipment(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			EquimedDAO equimedDao = new EquimedDAOImpl();
			List<Wypozyczenie> equipmentLoanList = equimedDao.findLoanEquipment();
			request.setAttribute("equipmentLoanList", equipmentLoanList);

		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	private void nextID(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			EquimedDAO equimedDao = new EquimedDAOImpl();
			int maxID = equimedDao.nextID();
			request.setAttribute("maxID", maxID);
		} catch (Exception e) {
		System.out.println(e);
		}
	}
}
