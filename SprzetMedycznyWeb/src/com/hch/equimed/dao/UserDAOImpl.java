package com.hch.equimed.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.hch.equimed.model.Uzytkownik;

import oracle.jdbc.OraclePreparedStatement;

public class UserDAOImpl implements UserDAO {

	static DBMySQL dbcon = new DBMySQL();
	static int log;

	@Override
	public boolean addUser(String login, String imie, String nazwisko, String telefon, String email, int id_oddzialu,
			String pesel, String prawoDostepu) {
		Connection connection = null;
		try {
			connection = DBOracle.connect();
			OraclePreparedStatement statement = (OraclePreparedStatement) connection.prepareStatement(
					"INSERT INTO uzytkownik (login, imie, nazwisko, telefon, email, id_oddzialu, pesel, prawoDostepu) values (?, ?, ?, ?, ?, ?, ?, ?)");
			statement.setString(1, login);
			statement.setString(2, imie);
			statement.setString(3, nazwisko);
			statement.setString(4, telefon);
			statement.setString(5, email);
			statement.setInt(6, id_oddzialu);
			statement.setString(7, pesel);
			statement.setString(8, prawoDostepu);
			statement.execute();
			System.out.println(statement);
		} catch (Exception e) {
			System.err.println("Blad przy wstawianiu uzytkownika");
			e.printStackTrace();
			return false;
		} finally {
			DBOracle.closeConnection(connection);
		}

		return true;
	}

	@Override
	public boolean addPassword(String login, String haslo) {

		Connection connection = null;
		try {
			connection = dbcon.getConnection();
			PreparedStatement statement = (PreparedStatement) connection
					.prepareStatement("INSERT INTO 'logowanie' ('login', 'haslo') values (?, ?)");
			statement.setString(1, login);
			statement.setString(2, haslo);
			statement.execute();
			System.out.println(statement);
		} catch (Exception e) {
			System.err.println("Blad przy wstawianiu hasla");
			e.printStackTrace();
			return false;
		} finally {
			dbcon.closeConnection(connection);
		}
		return true;
	}

	@Override
	public List<Uzytkownik> findAllUser() {
		List<Uzytkownik> result = new ArrayList<>();

		String sql = "select * from uzytkownik order by login ASC";

		Connection connection = null;

		try {
			connection = DBOracle.connect();
			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Uzytkownik uzytkownik = new Uzytkownik();
				uzytkownik.setLogin(resultSet.getString("login"));
				uzytkownik.setImie(resultSet.getString("imie"));
				uzytkownik.setNazwisko(resultSet.getString("nazwisko"));
				uzytkownik.setPesel(resultSet.getString("PESEL"));
				uzytkownik.setTelefon(resultSet.getString("telefon"));
				uzytkownik.setEmail(resultSet.getString("email"));
				uzytkownik.setPrawoDostepu(resultSet.getString("prawoDostepu"));
				uzytkownik.setID_oddzialu_FK(resultSet.getInt("id_oddzialu"));
				result.add(uzytkownik);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			DBOracle.closeConnection(connection);
		}
		return result;
	}

	@Override
	public void deleteUser(String login) {

		String sql = "delete from uzytkownik where login='" + login + "'";

		DBOracle.connectOracle(sql);
	}

	public static boolean checkUser(String login, String haslo) {
		String sql = "select count(*) from logowanie where login='" + login + "' and haslo = '" + haslo + "'";

		Connection connection = null;
		try {
			connection = dbcon.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet result = statement.executeQuery();
			result.next();
			log = Integer.parseInt(result.getObject(1).toString());
		} catch (Exception e) {
			System.out.println("B��d przy pobieraniu ilo�ci uzytkownikow z bazy " + e.toString());
			System.exit(3);
		} finally {
			dbcon.closeConnection(connection);
		}
		if (log == 1) {
			return true;
		} else {
			return false;
		}
	}
}
