<%@ page language="java" contentType="text/html; charset=ISO-8859-2"
	pageEncoding="ISO-8859-2"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.hch.equimed.model.*"%>
<!DOCTYPE html >
<%
	String param1 = application.getInitParameter("param1");
	String param2 = application.getInitParameter("param2");
	String param3 = application.getInitParameter("param3");
	String imageURL = application.getInitParameter("imageURL");
%>
<%
	//allow access only if session exists
	String user = (String) session.getAttribute("user");
	String userName = null;
	String sessionID = null;
	Cookie[] cookies = request.getCookies();
	if (cookies != null) {
		for (Cookie cookie : cookies) {
			if (cookie.getName().equals("user"))
				userName = cookie.getValue();
			if (cookie.getName().equals("JSESSIONID"))
				sessionID = cookie.getValue();
		}
	}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link rel="stylesheet" href="css/equi.css" type="text/css" />
<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
<script src="js/equi.js"></script>
</head>
<body>
	<div id="centered">

		<%
			String access = userName.toUpperCase().substring(0, 1);
			switch (access) {
			case "A":
		%>
		<jsp:include page="header.jsp" flush="true" />
		<%
			break;
			case "S":
		%>
		<jsp:include page="header_serwis.jsp" flush="true" />
		<%
			break;
			case "L":
		%>
		<jsp:include page="header_lp.jsp" flush="true" />
		<%
			break;
			case "I":
		%>
		<jsp:include page="header_inw.jsp" flush="true" />
		<%
			break;
			}
		%>


		<%
			List<Uzytkownik> users = (List<Uzytkownik>) request.getAttribute("userList");
			List<Oddzial> branch = (List<Oddzial>) request.getAttribute("branchList");
		%>
		<div class="container" style="width: 1200px;">

			<div class="row">
				<div class="col-lg-4">
					<span class="label"
						style="font-size: 20px; background-color: #99CCFF; color: #003366; font-style: italic;">
						Witaj! Jeste� zalogowany jako: <%
						for (Uzytkownik uzytkownik : users) {
							if (uzytkownik.getLogin().equals(userName.toUpperCase())) {
					%><%=uzytkownik.getLogin()%> <%
 	}
 	}
 %>
					
				</div>
				<div class="col-lg-8"></div>
			</div>
			</br>
			<div class="row">
				<div class="col-lg-4">
					<span class="label"
						style="font-size: 20px; background-color: #99CCFF; color: #003366;">Twoje
						dane</span>
				</div>
				<div class="col-lg-8"></div>
			</div>
			</br>
			<div class="col-lg-5"
				style="padding-top: 10px; padding-bottom: 10px; padding-left: 20px; background-color: #003366; border: 1px solid #99CCFF;">
				<div class="row">
					<div class="col-lg-5"
						style="font-size: 13px; color: #003366; font-family: verdana, monospace;">--</div>
					<div class="col-lg-7"
						style="font-size: 13px; color: #003366; font-family: verdana, monospace;">--</div>
				</div>
				<div class="row">
					<div class="col-lg-5"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-style: italic;">Login:</div>
					<div class="col-lg-7"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-weight: bold;"><%=userName.toUpperCase()%></div>
				</div>
				</br>
				<div class="row">
					<div class="col-lg-5"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-style: italic;">Imi�:</div>
					<div class="col-lg-7"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-weight: bold;">
						<%
							for (Uzytkownik uzytkownik : users) {
								if (uzytkownik.getLogin().equals(userName.toUpperCase())) {
						%><%=uzytkownik.getImie()%>

						<%
							}
							}
						%>
					</div>
				</div>
				</br>
				<div class="row">
					<div class="col-lg-5"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-style: italic;">Nazwisko:</div>
					<div class="col-lg-7"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-weight: bold;">
						<%
							for (Uzytkownik uzytkownik : users) {
								if (uzytkownik.getLogin().equals(userName.toUpperCase())) {
						%><%=uzytkownik.getNazwisko()%>

						<%
							}
							}
						%>
					</div>
				</div>
				</br>
				<div class="row">
					<div class="col-lg-5"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-style: italic;">Numer
						telefonu:</div>
					<div class="col-lg-7"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-weight: bold;">
						<%
							for (Uzytkownik uzytkownik : users) {
								if (uzytkownik.getLogin().equals(userName.toUpperCase())) {
						%><%=uzytkownik.getTelefon()%>

						<%
							}
							}
						%>
					</div>
				</div>
				</br>
				<div class="row">
					<div class="col-lg-5"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-style: italic;">E-mail::</div>
					<div class="col-lg-7"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-weight: bold;">
						<%
							for (Uzytkownik uzytkownik : users) {
								if (uzytkownik.getLogin().equals(userName.toUpperCase())) {
						%><%=uzytkownik.getEmail()%>
						<%
							}
							}
						%>
					</div>
				</div>
				</br>
				<div class="row">
					<div class="col-lg-5"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-style: italic;">Oddzia�:</div>
					<div class="col-lg-7"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-weight: bold;">
						<%
							for (Uzytkownik uzytkownik : users) {
								for (Oddzial oddzial : branch) {
									if (uzytkownik.getLogin().equals(userName.toUpperCase())) {
										if (uzytkownik.getID_oddzialu_FK() == (oddzial.getID_oddzialu())) {
						%><%=oddzial.getNazwa()%>

						<%
							}
									}
								}
							}
						%>
					</div>
				</div>
				</br>
				<div class="row">
					<div class="col-lg-5"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-style: italic;">PESEL:</div>
					<div class="col-lg-7"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-weight: bold;">
						<%
							for (Uzytkownik uzytkownik : users) {
								if (uzytkownik.getLogin().equals(userName.toUpperCase())) {
						%><%=uzytkownik.getPesel()%>

						<%
							}
							}
						%>
					</div>
				</div>
				</br>
				<div class="row">
					<div class="col-lg-5"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-style: italic;">Prawo
						dost�pu:</div>
					<div class="col-lg-7"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-weight: bold;">
						<%
							for (Uzytkownik uzytkownik : users) {
								if (uzytkownik.getLogin().equals(userName.toUpperCase())) {
						%><%=uzytkownik.getPrawoDostepu()%>

						<%
							}
							}
						%>
					</div>
				</div>
				</br>
			</div>
		</div>
	</div>
</body>
</html>