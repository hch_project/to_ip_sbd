<%@ page language="java" contentType="text/html; charset=ISO-8859-2"
	pageEncoding="ISO-8859-2"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.hch.equimed.model.*"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<!DOCTYPE html >
<%
	String param3 = application.getInitParameter("param3");
	SimpleDateFormat simpleDateHere = new SimpleDateFormat("yyyy-MM-dd");
	System.out.println(simpleDateHere.format(new Date()));

	String body = null;
	String onload = null;
	body = request.getParameter("progress");

	switch (body) {
	case "list":
		onload = "";
		break;
	case "add":
		onload = "window.alert('Doda�e� sprz�t')";
		break;
	case "change":
		onload = "window.alert('Edytowa�e� numer!')";
		break;
	case "damage":
		onload = "window.alert('Zg�osi�e� awari�!')";
		break;
	}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<script src="js/jquery-1.9.1.js"></script>
<link rel="stylesheet" href="css/equi.css" type="text/css" />
<script src="js/equi.js" type="text/javascript"></script>
<link href="css/bootstrap.min.css" rel="stylesheet">
</head>

<body onload="<%=onload%>">
	<div id="centered">

		<jsp:include page="header_inw.jsp" flush="true" />
		<div class="container" style="width: 1200px;">
			<div class="row">
				<div class="col-lg-4"></div>
				<div class="col-lg-4">
					<span class="label"
						style="font-size: 20px; background-color: #99CCFF; color: #003366;">Lista
						ca�ego sprz�tu</span>
				</div>
				<div class="col-lg-4"></div>
			</div>
			</br>
			<div class="row">
				<div class="col-lg-1"
					style="height: 65px; background-color: #99CCFF; color: #003366;">ID
					sprz�tu</div>
				<div class="col-lg-2"
					style="height: 65px; background-color: #99CCFF; color: #003366;">Nazwa</div>
				<div class="col-lg-1"
					style="height: 65px; background-color: #99CCFF; color: #003366;">Producent</div>
				<div class="col-lg-1"
					style="height: 65px; background-color: #99CCFF; color: #003366;">Nr
					inwen- tarzowy</div>
				<div class="col-lg-1"
					style="height: 65px; background-color: #99CCFF; color: #003366;">Zakup</div>
				<div class="col-lg-1"
					style="height: 65px; background-color: #99CCFF; color: #003366;">Wprowa-
					dzono</div>
				<div class="col-lg-1"
					style="height: 65px; background-color: #99CCFF; color: #003366;">Gwarancja</div>
				<div class="col-lg-2"
					style="height: 65px; background-color: #99CCFF; color: #003366;">Oddzia�</div>
				<div class="col-lg-1"
					style="height: 65px; background-color: #99CCFF; color: #003366;">Edytuj
					nr inw.</div>
				<div class="col-lg-1"
					style="height: 65px; background-color: #99CCFF; color: #003366;">Serwis</div>

			</div>
			<div class="row">
				<%
					List<Oddzial> branch = (List<Oddzial>) request.getAttribute("branchList");
					List<Sprzet> brands = (List<Sprzet>) request.getAttribute("equipmentList");

					for (Sprzet sprzet : brands) {
				%>
				<div class="col-lg-1" style="height: 85px;"><%=sprzet.getNr_ID()%></div>
				<div class="col-lg-2" style="height: 85px;"><%=sprzet.getNazwa()%></div>
				<div class="col-lg-1" style="height: 85px;"><%=sprzet.getProducent()%></div>
				<div class="col-lg-1" style="height: 85px;"><%=sprzet.getNr_in()%></div>
				<div class="col-lg-1" style="height: 85px;">
					<%
						Date date_zakupu = simpleDateHere.parse(sprzet.getDataZakupu());
					%><%=simpleDateHere.format(date_zakupu)%></div>
				<div class="col-lg-1" style="height: 85px;">
					<%
						Date dw = simpleDateHere.parse(sprzet.getDw());
					%><%=simpleDateHere.format(dw)%></div>
				<div class="col-lg-1" style="height: 85px;">
					<%
						Date gwarancja = simpleDateHere.parse(sprzet.getGwarancja());
					%><%=simpleDateHere.format(gwarancja)%></div>
				<div class="col-lg-2" style="height: 85px;">
					<%
						for (Oddzial oddzial : branch) {
								if (sprzet.getID_oddzialu() == (oddzial.getID_oddzialu())) {
					%><%=oddzial.getNazwa()%>

					<%
						}
							}
					%>
				</div>
				<div class="col-lg-1" style="height: 85px;">
					<form class="label">
						<button class="btn btn-primary btn-xs" type="submit">
							<a
								href="<%=param3%>?nr_id=<%=sprzet.getNr_ID()%>&action=editNrIn"
								style="color: white;"> <i class="fa fa-pencil"></i> Edytuj
							</a>
						</button>
					</form>

				</div>
				<div class="col-lg-1" style="height: 85px;">

					<form class="label">
						<button class="btn btn-primary btn-xs" type="submit">
							<a
								href="<%=param3%>?nr_id=<%=sprzet.getNr_ID()%>&action=damage_I&progress=damage"
								style="color: white;"> <i class="fa fa-exclamation-triangle"></i>
								Awaria
							</a>
						</button>
					</form>
				</div>
				<%
					}
				%>

			</div>
		</div>

	</div>


</body>
</html>

