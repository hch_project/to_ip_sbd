<%@ page language="java" contentType="text/html; charset=ISO-8859-2"
	pageEncoding="ISO-8859-2"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.hch.equimed.model.*"%>
<%@ page import="java.io.*,java.util.*, javax.servlet.*"%>
<%@page import="java.text.SimpleDateFormat"%>


<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<script src="js/jquery-1.9.1.js"></script>
<link rel="stylesheet" href="css/equi.css" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet">
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<script src="js/equi.js" type="text/javascript"></script>

</head>
<body>
	<div id="centered">

		<jsp:include page="header_serwis.jsp" flush="true" />

		<div class="container">
			<div class="row">
				<div class="col-lg-4"></div>
				<div class="col-lg-4">
					<span class="label"
						style="font-size: 20px; background-color: #99CCFF; color: #003366;">Dodawanie
						opisu naprawy</span>
				</div>
				<%
					List<Sprzet> equipment = (List<Sprzet>) request.getAttribute("equipmentList");
					List<Oddzial> branch = (List<Oddzial>) request.getAttribute("branchList");
				%>
				<div class="col-lg-4"></div>
			</div>
			</br>
			<div class="row">
				<div class="col-lg-3"></div>
				<div class="col-lg-6"
					style="padding-top: 10px; padding-bottom: 10px; padding-left: 20px; background-color: #003366;">
					<form role="form">
						<input type="hidden" name="action" value="addRepair" />
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Numer
								ID</label> <input style="width: 400px; font-family: verdana, monospace;"
								readonly="readonly" type="text" class="form-control" id=nr_id
								value="<%=request.getParameter("numerID")%>" name="nr_id">

						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Numer
								serwisowy</label> <input
								style="width: 400px; font-family: verdana, monospace;"
								type="author" class="form-control" id="nr_sr"
								placeholder="Podaj numer serwisowy" name="nr_sr">
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Data
								naprawy</label> <input
								style="width: 400px; font-family: verdana, monospace;"
								readonly="readonly" type="text" class="form-control" id="data"
								value="<%Date dNow = new Date();
			SimpleDateFormat ft = new SimpleDateFormat(" yyyy-MM-dd ");
			out.print(ft.format(dNow));%>
"
								name="data">
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Opis
								naprawy</label> <input
								style="width: 400px; font-family: verdana, monospace;"
								type="text" class="form-control" id="opis"
								placeholder="Podaj opis naprawy..." name="opis">
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Oddzia�,
								na kt�ry trafia sprz�t</label> <input
								style="width: 400px; font-family: verdana, monospace;"
								disabled="disabled" type="text" class="form-control"
								id="oddzial"
								value="<%for (Sprzet sprzet : equipment) {
				for (Oddzial oddzial : branch) {
					long id = Long.parseLong(request.getParameter("numerID"));
					if (sprzet.getNr_ID() == id) {
						if (sprzet.getID_oddzialu() == (oddzial.getID_oddzialu())) {%><%=oddzial.getNazwa()%>

							<%}
					}
				}
			}%>"
								name="id_oddzialu_w">

							<%
								for (Sprzet sprzet : equipment) {
									for (Oddzial oddzial : branch) {
										long id = Long.parseLong(request.getParameter("numerID"));
										if (sprzet.getNr_ID() == id) {
											if (sprzet.getID_oddzialu() == (oddzial.getID_oddzialu())) {
												long id_oddz = oddzial.getID_oddzialu();
							%>
							<input type="hidden" name="id_oddzialu" value="<%=id_oddz%>" />
							<%
								}
										}
									}
								}
							%>
						</div>
						<input type="hidden" class="form-control" name="progress"
							value="add">
						<button type="submit" class="btn btn-primary" value="addRepair">Wy�lij</button>
					</form>
				</div>
				<div class="col-lg-3"></div>
			</div>
		</div>
</body>
</html>