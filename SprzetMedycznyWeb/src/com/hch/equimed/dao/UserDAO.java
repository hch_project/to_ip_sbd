package com.hch.equimed.dao;

import java.util.List;

import com.hch.equimed.model.Uzytkownik;

public interface UserDAO {

	public boolean addUser(String login, String imie, String nazwisko, String telefon, String email, int id_oddzialu,
			String pesel, String prawoDostepu);

	public boolean addPassword(String login, String haslo);

	public List<Uzytkownik> findAllUser();

	public void deleteUser(String login);

	public static boolean checkUser(String login, String haslo) {
		return false;
	}
}
