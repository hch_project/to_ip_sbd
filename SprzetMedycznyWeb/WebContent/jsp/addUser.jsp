<%@ page language="java" contentType="text/html; charset=ISO-8859-2"
	pageEncoding="ISO-8859-2"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.hch.equimed.model.*"%>


<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<script src="js/jquery-1.9.1.js"></script>
<link rel="stylesheet" href="css/equi.css" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet">
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<script src="js/equi.js" type="text/javascript"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<style>
input.invalid, textarea.invalid {
	border: 3px solid red;
}

.blad {
	color: red;
}
</style>
</head>
<body>
	<div id="centered">

		<jsp:include page="header.jsp" flush="true" />

		<div class="container">
			<div class="row">
				<div class="col-lg-4"></div>
				<div class="col-lg-4">
					<span class="label"
						style="font-size: 20px; background-color: #99CCFF; color: #003366;">Dodawanie
						u�ytkownika</span>
				</div>
				<div class="col-lg-4"></div>
			</div>
			</br>
			<div class="row">
				<div class="col-lg-3"></div>
				<div class="col-lg-6"
					style="padding-top: 10px; padding-bottom: 10px; padding-left: 20px; background-color: #003366;">
					<form role="form">
						<input type="hidden" name="action" value="add" />
						<div class="form-group">
							<label for="login"
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Login</label>
							<input style="width: 400px; font-family: verdana, monospace;"
								type="author" class="form-control" id="login"
								placeholder="Podaj LOGIN" name="login" required="required"><span
								class="komunikat"></span>
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Has�o</label>
							<input style="width: 400px; font-family: verdana, monospace;"
								type="password" class="form-control" id="haslo"
								placeholder="Podaj HAS�O" name="haslo" required="required">
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Imi�</label>
							<input style="width: 400px; font-family: verdana, monospace;"
								type="author" class="form-control" id="imie"
								placeholder="Podaj IMI�" name="imie" required="required"><span
								class="komunikat"></span>
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Nazwisko</label>
							<input style="width: 400px; font-family: verdana, monospace;"
								type="author" class="form-control" id="nazwisko"
								placeholder="Podaj NAZWISKO" name="nazwisko" required="required"><span
								class="komunikat"></span>
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Telefon</label>
							<input style="width: 400px; font-family: verdana, monospace;"
								maxlength="9" type="text" class="form-control" id="telefon"
								placeholder="Podaj TELEFON" name="telefon" required="required"><span
								class="komunikat"></span>
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">E-mail</label>
							<input style="width: 400px; font-family: verdana, monospace;"
								type="email" class="form-control" id="email"
								placeholder="Podaj E-MAIL" name="email" required="required">
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">PESEL</label>
							<input style="width: 400px; font-family: verdana, monospace;"
								minlength="11" maxlength="11" type="text" class="form-control"
								id="pesel" placeholder="Podaj PESEL" name="pesel"
								required="required"><span class="komunikat"></span>
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Prawo
								dost�pu</label> <select class="form-control"
								style="width: 400px; font-family: verdana, monospace;"
								name="prawoDostepu">
								<option value="Admin">Admin</option>
								<option value="Lekarz/piel�gniarka">Lekarz/piel�gniarka</option>
								<option value="Serwis">Serwis</option>
								<option value="Sekcja inwentaryzacji">Sekcja
									inwentaryzacji</option>
							</select>
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Oddzia�</label>
							<select class="form-control"
								style="width: 400px; font-family: verdana, monospace;"
								name="id_oddzialu">
								<option value="101">101 - Oddzia� Neurologiczny</option>
								<option value="102">102 - Oddzia� Noworodkowy</option>
								<option value="103">103 - Oddzia� Ginekologiczno -
									Po�o�niczy</option>
								<option value="104">104 - Oddzia� Rehabilitacji</option>
								<option value="105">105 - Oddzia� Kardiologii</option>
								<option value="106">106 - Szpitalny Oddzia� Ratunkowy</option>
								<option value="107">107 - Oddzia� Urazowo-Ortopedyczny</option>
								<option value="108">108 - Oddzia� Chor�b Wewn�trznych</option>
								<option value="109">109 - Oddzia� Kardiochirurgii</option>
								<option value="110">110 - Oddzia� Transplantologii</option>
								<option value="111">111 - Oddzia� Chirurgii Og�lnej i
									Ma�oinwazyjnej</option>
								<option value="112">112 - Oddzia� Intensywnej Terapii i
									Anestezjologii</option>
								<option value="113">113 - Blok Operacyjny</option>
								<option value="114">114 - Administracja</option>
								<option value="115">115 - Aparatura medyczna</option>
							</select>
						</div>
						<input type="hidden" class="form-control" name="progress"
							value="add">
						<button type="submit" class="btn btn-primary" value="Dodaj">
							<i class="fa fa-plus"></i>&nbsp; Dodaj
						</button>
					</form>
				</div>
				<div class="col-lg-3"></div>
			</div>
		</div>
</body>
</html>