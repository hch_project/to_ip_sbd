package com.hch.equimed.model;

public class Wypozyczenie {
	private long id_wyp;
	private int id_oddzialu_w;
	private int id_oddzialu_p;
	private String data_p;
	private String data_k;
	private long nr_ID;

	public long getID_wyp() {
		return id_wyp;
	}

	public void setID_wyp(long iD_wyp) {
		id_wyp = iD_wyp;
	}

	public int getID_oddzialu_w() {
		return id_oddzialu_w;
	}

	public void setID_oddzialu_w(int iD_oddzialu_w) {
		id_oddzialu_w = iD_oddzialu_w;
	}

	public int getID_oddzialu_p() {
		return id_oddzialu_p;
	}

	public void setID_oddzialu_p(int iD_oddzialu_p) {
		id_oddzialu_p = iD_oddzialu_p;
	}

	public String getData_p() {
		return data_p;
	}

	public void setData_p(String data_p) {
		this.data_p = data_p;
	}

	public String getData_k() {
		return data_k;
	}

	public void setData_k(String data_k) {
		this.data_k = data_k;
	}

	public long getNr_ID() {
		return nr_ID;
	}

	public void setNr_ID(long nr_ID) {
		this.nr_ID = nr_ID;
	}

	public String toString() {
		return "ID wypozyczenia: " + id_wyp + " ID oddzialu wypozyczajacego: " + id_oddzialu_w
				+ " ID oddzialu pozyczajacego: " + id_oddzialu_p;
	}
}
