<%@ page language="java" contentType="text/html; charset=ISO-8859-2"
	pageEncoding="ISO-8859-2"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.hch.equimed.model.*"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>

<!DOCTYPE html >
<%
	String param1 = application.getInitParameter("param1");
	String param3 = application.getInitParameter("param3");
	SimpleDateFormat simpleDateHere = new SimpleDateFormat("yyyy-MM-dd");
	String body = null;
	String onload = null;
	body = request.getParameter("progress");

	switch (body) {
	case "list":
		onload = "";
		break;
	case "delete":
		onload = "window.alert('Odda�e� sprz�t!')";
		break;
	}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<script src="js/jquery-1.9.1.js"></script>
<link rel="stylesheet" href="css/equi.css" type="text/css" />
<script src="js/equi.js" type="text/javascript"></script>
<link href="css/bootstrap.min.css" rel="stylesheet">
</head>

<body onload="<%=onload%>">
	<div id="centered">

		<jsp:include page="header_lp.jsp" flush="true" />
		<div class="container" style="width: 1200px;">
			<div class="row">
				<div class="col-lg-4"></div>
				<div class="col-lg-4">
					<span class="label"
						style="font-size: 20px; background-color: #99CCFF; color: #003366;">Lista
						wypo�yczonego sprz�tu</span>
				</div>
				<div class="col-lg-4"></div>
			</div>
			</br>
			<div class="row">
				<div class="col-lg-2"
					style="height: 65px; background-color: #99CCFF; color: #003366;">ID_wypo�yczenia</div>
				<div class="col-lg-2"
					style="height: 65px; background-color: #99CCFF; color: #003366;">Oddzia�
					wypo�yczaj�cy</div>
				<div class="col-lg-2"
					style="height: 65px; background-color: #99CCFF; color: #003366;">Oddzia�
					po�yczaj�cy</div>
				<div class="col-lg-2"
					style="height: 65px; background-color: #99CCFF; color: #003366;">Data
					pocz�tkowa</div>
				<div class="col-lg-2"
					style="height: 65px; background-color: #99CCFF; color: #003366;">Data
					ko�cowa</div>
				<div class="col-lg-1"
					style="height: 65px; background-color: #99CCFF; color: #003366;">ID
					sprz�tu</div>
				<div class="col-lg-1"
					style="height: 65px; background-color: #99CCFF; color: #003366;"></div>

			</div>
			<div class="row">

				<%
					List<Oddzial> branch = (List<Oddzial>) request.getAttribute("branchList");
					List<Wypozyczenie> loanEquip = (List<Wypozyczenie>) request.getAttribute("equipmentLoanList");

					for (Wypozyczenie wypozyczenie : loanEquip) {
				%>
				<div class="col-lg-2" style="height: 65px;"><%=wypozyczenie.getID_wyp()%></div>
				<div class="col-lg-2" style="height: 65px;">
					<%
						for (Oddzial oddzial : branch) {
								if (wypozyczenie.getID_oddzialu_w() == (oddzial.getID_oddzialu())) {
					%><%=oddzial.getNazwa()%>

					<%
						}
							}
					%>
				</div>
				<div class="col-lg-2" style="height: 65px;">
					<%
						for (Oddzial oddzial : branch) {
								if (wypozyczenie.getID_oddzialu_p() == (oddzial.getID_oddzialu())) {
					%><%=oddzial.getNazwa()%>

					<%
						}
							}
					%>
				</div>
				<div class="col-lg-2" style="height: 65px;">
					<%
						Date data_p = simpleDateHere.parse(wypozyczenie.getData_p());
					%><%=simpleDateHere.format(data_p)%></div>
				<div class="col-lg-2" style="height: 65px;">
					<%
						Date data_k = simpleDateHere.parse(wypozyczenie.getData_k());
					%><%=simpleDateHere.format(data_k)%></div>
				<div class="col-lg-1" style="height: 65px;"><%=wypozyczenie.getNr_ID()%></div>
				<div class="col-lg-1" style="height: 65px;">

					<form class="label">
						<input type="hidden" name="nr_id"
							value="<%=wypozyczenie.getNr_ID()%>" /> <input type="hidden"
							class="form-control" name="progress" value="delete" />
						<button class="btn btn-primary btn-xs" type="submit" name="action"
							value="deleteLoan">
							<i class="fa fa-times"></i> Oddaj
						</button>
					</form>
				</div>
				<%
					}
				%>

			</div>
		</div>

	</div>


</body>
</html>

