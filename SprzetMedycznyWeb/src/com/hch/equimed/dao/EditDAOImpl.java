package com.hch.equimed.dao;

public class EditDAOImpl implements EditDAO {

	static DBMySQL dbcon = new DBMySQL();

	@Override
	public void updateUser(String login, String haslo, String nazwisko, String telefon, String email, int id_oddzialu) {
		String sql = null;

		if (!haslo.isEmpty()) {
			sql = "update logowanie set haslo='" + haslo + "' where login='" + login + "'";
			dbcon.connect(sql);
		}
		if (!nazwisko.isEmpty()) {
			sql = "update uzytkownik set nazwisko='" + nazwisko + "' where login='" + login + "'";
			DBOracle.connectOracle(sql);
		}
		if (!telefon.isEmpty()) {
			sql = "update uzytkownik set telefon='" + telefon + "' where login='" + login + "'";
			DBOracle.connectOracle(sql);
		}
		if (!email.isEmpty()) {
			sql = "update uzytkownik set email='" + email + "' where login='" + login + "'";
			DBOracle.connectOracle(sql);
		}
		if (id_oddzialu != 0) {
			sql = "update uzytkownik set id_oddzialu='" + id_oddzialu + "' where login='" + login + "'";
			DBOracle.connectOracle(sql);
		}
	}

	@Override
	public void changeStatus(int nr_id, int awaria, int wypozyczenie) {
		String sql = null;

		if (awaria != 0) {
			sql = "update sprzet set awaria='" + awaria + "' where nr_id='" + nr_id + "'";
			DBOracle.connectOracle(sql);
		}
		if (wypozyczenie != 0) {
			sql = "update sprzet set wypozyczenie='" + wypozyczenie + "' where nr_id='" + nr_id + "'";
			DBOracle.connectOracle(sql);
		}
	}

	@Override
	public void changeNrIn(int nr_id, int nr_in) {
		String sql = null;
		sql = "update sprzet set nr_in='" + nr_in + "' where nr_id='" + nr_id + "'";
		DBOracle.connectOracle(sql);
	}

	@Override
	public void changeDate(int nr_id, String ostatni_przeglad) {
		String sql = null;
		sql = "update sprzet set ostatni_przeglad=to_date('" + ostatni_przeglad + "','yyyy-mm-dd') where nr_id='"
				+ nr_id + "'";
		DBOracle.connectOracle(sql);
	}
}
