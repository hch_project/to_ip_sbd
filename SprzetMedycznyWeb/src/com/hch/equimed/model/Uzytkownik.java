package com.hch.equimed.model;

public class Uzytkownik {
	private String login;
	private String haslo;
	private String imie;
	private String nazwisko;
	private String telefon;
	private String email;
	private int id_oddzialu;
	private String pesel;
	private String prawoDostepu;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getID_oddzialu_FK() {
		return id_oddzialu;
	}

	public void setID_oddzialu_FK(int id_oddzialu) {
		this.id_oddzialu = id_oddzialu;
	}

	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	public String getPrawoDostepu() {
		return prawoDostepu;
	}

	public void setPrawoDostepu(String prawoDostepu) {
		this.prawoDostepu = prawoDostepu;
	}

	public String toString() {
		return "Login: " + login + " Imi�: " + imie + " Nazwisko: " + nazwisko + " Tel: " + telefon + " Email: " + email
				+ " PESEL: " + pesel;
	}

	public String getHaslo() {
		return haslo;
	}

	public void setHaslo(String haslo) {
		this.haslo = haslo;
	}
}
