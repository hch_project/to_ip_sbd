/**
 * 
 */
package com.hch.equimed.test;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.List;

import org.junit.Test;

import com.hch.equimed.dao.DBOracle;
import com.hch.equimed.dao.EditDAOImpl;
import com.hch.equimed.dao.EquimedDAOImpl;
import com.hch.equimed.model.Oddzial;
import com.hch.equimed.model.Sprzet;
import com.hch.equimed.model.Wypozyczenie;

/**
 * @author Doubler
 *
 */
public class Test_Equimed extends EquimedDAOImpl {

	// @Before
	static EditDAOImpl editDao = new EditDAOImpl();
	String login = "blabla";
	String imie = "test_imie";
	String nazwisko = "test_nazwisko";
	String telefon = "123456";
	String email = "test_email";
	String pesel = "145236";
	String prawoDostepu = "1";

	int id_oddzialu = 101;
	int id_wyp = 100;
	int id_oddzialu_w = 101;
	int id_oddzialu_p = 108;
	int nr_id = 600;
	int nr_in = 600;
	int nr_sr = 600;
	String data = "2016-04-15";
	String data_p = "2016-01-04";
	String data_k = "2016-02-06";
	String nazwa = "EKG 15";
	String producent = "SECA";
	String data_zakupu = "2016-01-03";
	String dw = "2016-01-05";
	String gwarancja = "2018-01-03";
	String ostatni_przeglad = "2016-01-04";
	String opis = "Test zosta� wykonany";

	/**
	 * Test method for
	 * {@link com.hch.equimed.dao.EquimedDAOImpl#addEquip(java.lang.String)}
	 * .
	 */	
	@Test
	public void testAddEquip() throws SQLException {
		String sql = "delete from sprzet where nr_id='" + nr_id + "'";
		DBOracle.connectOracle(sql);
		assertTrue(addEquip(nr_id, nazwa, producent, nr_in, data_zakupu, dw, gwarancja, ostatni_przeglad, id_oddzialu));
		assertFalse(
				addEquip(nr_id, nazwa, producent, nr_in, data_zakupu, dw, gwarancja, ostatni_przeglad, id_oddzialu));
	}

	/**
	 * Test method for
	 * {@link com.hch.equimed.dao.EquimedDAOImpl#findAllBranch(java.lang.String)}
	 * .
	 */
	@Test
	public void testFindAllBranch() {
		List<Oddzial> oddzialy = findAllBranch();
		int i = 0;
		for (Oddzial oddzial : oddzialy) {
			assertTrue(oddzial.getID_oddzialu() == 101 + i);
			i++;
		}
	}

	/**
	 * Test method for
	 * {@link com.hch.equimed.dao.EquimedDAOImpl#findAllEquipment(java.lang.String)}
	 * .
	 */
	@Test
	public void testFindAllEquipment() {
		List<Sprzet> sprzety = findAllEquipment();
		for (Sprzet sprzet : sprzety) {
			assertTrue(!sprzet.getNazwa().isEmpty());
			assertTrue(!sprzet.getProducent().isEmpty());
			assertFalse(sprzet.getDataZakupu().isEmpty());
			assertFalse(sprzet.getGwarancja().isEmpty());
		}

	}

	/**
	 * Test method for
	 * {@link com.hch.equimed.dao.EquimedDAOImpl#searchEquipmentByKeyword(java.lang.String)}
	 * .
	 */
	@Test
	public void testSearchEquipmentByKeyword() {
		List<Sprzet> sprzety = searchEquipmentByKeyword("szukanafraza");
		List<Oddzial> oddzialy = findAllBranch();
		for (Sprzet sprzet : sprzety) {
			for (Oddzial oddzial : oddzialy) {
				assertTrue(sprzet.getNazwa().contains("szukanafraza") || sprzet.getProducent().contains("szukanafraza")
						|| sprzet.getNr_ID() == 1 || oddzial.getNazwa().contains("szukanafraza"));
			}
		}
	}

	/**
	 * Test method for
	 * {@link com.hch.equimed.dao.EquimedDAOImpl#findLoanEquipment(java.lang.String)}
	 * .
	 */
	@Test
	public void testFindLoanEquipment() {
		deleteLoanEquipment(nr_id);
		assertTrue(saveLoan(id_wyp, id_oddzialu_w, id_oddzialu_p, data_p, data_k, nr_id));
		assertFalse(saveLoan(id_wyp, id_oddzialu_w, id_oddzialu_p, data_p, data_k, nr_id));
		List<Wypozyczenie> wypozyczenia = findLoanEquipment();
		for (Wypozyczenie wyp : wypozyczenia) {
			assertTrue(wyp.getID_wyp() == id_wyp || wyp.getID_oddzialu_w() == id_oddzialu_w
					|| wyp.getID_oddzialu_p() == id_oddzialu_p);
		}
		deleteLoanEquipment(nr_id);
	}

	/**
	 * Test method for
	 * {@link com.hch.equimed.dao.EquimedDAOImpl#saveLoan(java.lang.String)}
	 * .
	 */
	@Test
	public void testSaveLoan() {
		deleteLoanEquipment(nr_id);
		assertTrue(saveLoan(id_wyp, id_oddzialu_w, id_oddzialu_p, data_p, data_k, nr_id));
		assertFalse(saveLoan(id_wyp, id_oddzialu_w, id_oddzialu_p, data_p, data_k, nr_id));
		List<Wypozyczenie> wypozyczenia = findLoanEquipment();
		for (Wypozyczenie wyp : wypozyczenia) {
			assertTrue(wyp.getID_wyp() == id_wyp);
		}
		deleteLoanEquipment(nr_id);
	}

	/**
	 * Test method for
	 * {@link com.hch.equimed.dao.EquimedDAOImpl#saveRepair(java.lang.String)}
	 * .
	 */
	@Test
	public void testSaveRepair() {
		editDao.changeStatus(nr_id, 1, 0);
		List<Sprzet> sprzety = findAllEquipment();
		for (Sprzet sprzet : sprzety) {
			if (sprzet.getNr_ID() == nr_id)
				assertTrue(sprzet.getAwaria() == 1);
		}
		saveRepair(nr_sr, opis, data, nr_id);
		List<Sprzet> sprzety1 = findAllEquipment();
		for (Sprzet sprzet : sprzety1) {
			if (sprzet.getNr_ID() == nr_id)
				assertTrue(sprzet.getAwaria() == 0);
		}
		String sql = "delete from naprawa where nr_sr='" + nr_sr + "'";
		DBOracle.connectOracle(sql);
	}

	/**
	 * Test method for
	 * {@link com.hch.equimed.dao.EquimedDAOImpl#deleteLoanEquipment(java.lang.String)}
	 * .
	 */
	@Test
	public void testDeleteLoanEquipment() throws SQLException {
		String sql = "delete from sprzet where nr_id='" + nr_id + "'";
		DBOracle.connectOracle(sql);
		assertTrue(addEquip(nr_id, nazwa, producent, nr_in, data_zakupu, dw, gwarancja, ostatni_przeglad, id_oddzialu));
		assertTrue(saveLoan(id_wyp, id_oddzialu_w, id_oddzialu_p, data_p, data_k, nr_id));
		editDao.changeStatus(nr_id, 0, 1);
		List<Wypozyczenie> wypozyczenia = findLoanEquipment();
		List<Sprzet> sprzety = findAllEquipment();
		for (Wypozyczenie wyp : wypozyczenia) {
			for (Sprzet sprzet : sprzety) {
				if (sprzet.getNr_ID() == nr_id) {
					assertTrue(sprzet.getWypozyczenie() == 1);
					assertTrue(wyp.getID_wyp() == id_wyp || wyp.getID_oddzialu_w() == id_oddzialu_w
							|| wyp.getID_oddzialu_p() == id_oddzialu_p);
				}
			}
		}
		deleteLoanEquipment(nr_id);
		List<Sprzet> sprzety1 = findAllEquipment();
		for (Wypozyczenie wyp : wypozyczenia) {
			for (Sprzet sprzet : sprzety1) {
				if (sprzet.getNr_ID() == nr_id) {
					assertTrue(sprzet.getWypozyczenie() == 0);
					assertTrue(!Long.toString(wyp.getID_wyp()).isEmpty());
					assertFalse(Long.toString(wyp.getID_wyp()).isEmpty());
				}
			}
		}
	}

	/**
	 * Test method for
	 * {@link com.hch.equimed.dao.EquimedDAOImpl#deleteEquipment(java.lang.String)}
	 * .
	 */
	@Test
	public void testDeleteEquipment() {
		deleteEquipment(nazwa);
		List<Sprzet> sprzety = searchEquipmentByKeyword(nazwa);
		for (Sprzet sprzet : sprzety) {
			assertNull(sprzet.getNazwa().contains(nazwa));
		}
	}

}
