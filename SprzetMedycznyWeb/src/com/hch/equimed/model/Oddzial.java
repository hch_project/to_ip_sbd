package com.hch.equimed.model;

public class Oddzial {
	private int ID_oddzialu;
	private String nazwa;

	public int getID_oddzialu() {
		return ID_oddzialu;
	}

	public void setID_oddzialu(int iD_oddzialu) {
		ID_oddzialu = iD_oddzialu;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public String toString() {
		return "ID: " + ID_oddzialu + " Oddzia�: " + nazwa;
	}
}
