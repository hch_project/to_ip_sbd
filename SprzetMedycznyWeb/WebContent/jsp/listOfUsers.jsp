<%@ page language="java" contentType="text/html; charset=ISO-8859-2"
	pageEncoding="ISO-8859-2"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.hch.equimed.model.*"%>
<!DOCTYPE html >
<%
	String param3 = application.getInitParameter("param3");
	String body = null;
	String onload = null;
	body = request.getParameter("progress");

	switch (body) {
	case "list":
		onload = "";
		break;
	case "add":
		onload = "window.alert('Doda�e� u�ytkownika')";
		break;
	case "delete":
		onload = "window.alert('Usun��e� u�ytkownika')";
		break;
	case "edit":
		onload = "window.alert('Edytowa�e� dane u�ytkownika')";
		break;
	}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<script src="js/jquery-1.9.1.js"></script>
<link rel="stylesheet" href="css/equi.css" type="text/css" />
<script src="js/equi.js" type="text/javascript"></script>
<link href="css/bootstrap.min.css" rel="stylesheet">
</head>

<body onload="<%=onload%>">
	<div id="centered">
		<jsp:include page="header.jsp" flush="true" />
		<div class="container" style="width: 1200px;">
			<div class="row">
				<div class="col-lg-4"></div>
				<div class="col-lg-4">
					<span class="label"
						style="font-size: 20px; background-color: #99CCFF; color: #003366;">Lista
						wszystkich u�ytkownik�w</span>
				</div>
				<div class="col-lg-4"></div>
			</div>
			</br>
		</div>
		<div class="row">
			<div class="col-lg-1"
				style="background-color: #99CCFF; color: #003366;">Login</div>
			<div class="col-lg-1"
				style="background-color: #99CCFF; color: #003366;">Imi�</div>
			<div class="col-lg-1"
				style="background-color: #99CCFF; color: #003366;">Nazwisko</div>
			<div class="col-lg-1"
				style="background-color: #99CCFF; color: #003366;">Telefon</div>
			<div class="col-lg-2"
				style="background-color: #99CCFF; color: #003366;">E-mail</div>
			<div class="col-lg-2"
				style="background-color: #99CCFF; color: #003366;">PESEL</div>
			<div class="col-lg-2"
				style="background-color: #99CCFF; color: #003366;">Oddzia�</div>
			<div class="col-lg-1"
				style="background-color: #99CCFF; color: #99CCFF;">E</div>
			<div class="col-lg-1"
				style="background-color: #99CCFF; color: #99CCFF;">U</div>

		</div>
		<div class="row">
			<%
				List<Uzytkownik> users = (List<Uzytkownik>) request.getAttribute("userList");
				List<Oddzial> branch = (List<Oddzial>) request.getAttribute("branchList");

				for (Uzytkownik uzytkownik : users) {
			%>
			<div class="col-lg-1"><%=uzytkownik.getLogin()%></div>
			<div class="col-lg-1"><%=uzytkownik.getImie()%></div>
			<div class="col-lg-1"><%=uzytkownik.getNazwisko()%></div>
			<div class="col-lg-1" style="padding-left: 7px;"><%=uzytkownik.getTelefon()%></div>
			<div class="col-lg-2"><%=uzytkownik.getEmail()%></div>
			<div class="col-lg-2"><%=uzytkownik.getPesel()%></div>
			<div class="col-lg-2">
				<%
					for (Oddzial oddzial : branch) {
							if (uzytkownik.getID_oddzialu_FK() == (oddzial.getID_oddzialu())) {
				%><%=oddzial.getNazwa()%>

				<%
					}
						}
				%>
			</div>
			<div class="col-lg-1" style="padding-top: 12px;">
				<form class="label">
					<button class="btn btn-primary btn-xs" type="submit">
						<a
							href="<%=param3%>?login=<%=uzytkownik.getLogin()%>&action=editUser"
							style="color: white;"> <i class="fa fa-pencil"></i> Edytuj
						</a>
					</button>

				</form>
			</div>
			<div class="col-lg-1" style="padding-top: 12px;">

				<form class="label">
					<input type="hidden" name="login"
						value="<%=uzytkownik.getLogin()%>" /> <input type="hidden"
						class="form-control" name="progress" value="delete">
					<button class="btn btn-primary btn-xs" type="submit" name="action"
						value="delete"
						onclick="window.history.pushState('Uzytkownik', 'Uzytkownik', 'http://localhost:8080/SprzetMedycznyWeb/user')">
						<i class="fa fa-trash-o"></i> Usu�
					</button>
				</form>
			</div>
			<%
				}
			%>
		</div>


	</div>
</body>
</html>

