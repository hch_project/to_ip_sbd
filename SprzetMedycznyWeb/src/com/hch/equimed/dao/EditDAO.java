package com.hch.equimed.dao;

public interface EditDAO {

	public void updateUser(String login, String haslo, String nazwisko, String telefon, String email, int id_oddzialu);

	public void changeStatus(int nr_id, int awaria, int wypozyczenie);

	public void changeNrIn(int nr_id, int nr_in);

	public void changeDate(int nr_id, String ostatni_przeglad);

}
