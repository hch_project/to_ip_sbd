package com.hch.equimed.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.hch.equimed.model.Uzytkownik;

public class Test_Uzytkownik extends Uzytkownik {

	@Test
	public void testSetLogin() {
		setLogin("Test");
		assertEquals("Test", getLogin());
	}

	@Test
	public void testSetImie() {
		setImie("Jan");
		assertEquals("Jan", getImie());
	}

	@Test
	public void testSetNazwisko() {
		setNazwisko("Nowak");
		assertEquals("Nowak", getNazwisko());
	}

	@Test
	public void testSetTelefon() {
		setTelefon("123456789");
		assertEquals("123456789", getTelefon());
	}

	@Test
	public void testSetEmail() {
		setEmail("test@test.pl");
		assertEquals("test@test.pl", getEmail());
	}

	@Test
	public void testSetID_oddzialu_FK() {
		setID_oddzialu_FK(101);
		assertEquals(101, getID_oddzialu_FK());
	}

	@Test
	public void testSetPesel() {
		setPesel("10203012345");
		assertEquals("10203012345", getPesel());
	}

	@Test
	public void testSetPrawoDostepu() {
		setPrawoDostepu("1");
		assertEquals("1", getPrawoDostepu());
	}

}
