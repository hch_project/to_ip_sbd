<%@page import="sun.invoke.empty.Empty"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-2"
	pageEncoding="ISO-8859-2"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.hch.equimed.model.*"%>


<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<script src="js/jquery-1.9.1.js"></script>
<link rel="stylesheet" href="css/equi.css" type="text/css" />
<script src="js/equi.js" type="text/javascript"></script>
<style>
input.invalid, textarea.invalid {
	border: 3px solid red;
}

.blad {
	color: red;
}
</style>
</head>
<body>
	<div id="centered">

		<jsp:include page="header.jsp" flush="true" />

		<div class="container">
			<div class="row">
				<div class="col-lg-4"></div>
				<div class="col-lg-4">
					<span class="label"
						style="font-size: 20px; background-color: #99CCFF; color: #003366;">Edycja
						danych</span>
				</div>
				<div class="col-lg-4"></div>
			</div>
			</br>
			<div class="row">
				<div class="col-lg-3"></div>
				<div class="col-lg-4"
					style="padding-top: 10px; padding-bottom: 10px; padding-left: 20px; background-color: #003366;">
					<form role="form">
						<input type="hidden" name="action" value="edit" /> <input
							type="hidden" name="login" id="login"
							value="<%=request.getParameter("login")%>" />
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Zmie�
								has�o</label> <input
								style="width: 350px; font-family: verdana, monospace;"
								type="password" class="form-control" id="haslo"
								placeholder="Podaj nowe HAS�O" name="haslo">
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Zmie�
								nazwisko</label> <input
								style="width: 350px; font-family: verdana, monospace;"
								type="author" class="form-control" id="nazwisko"
								placeholder="Podaj nowe NAZWISKO" name="nazwisko"><span
								class="komunikat"></span>
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Zmie�
								telefon</label> <input
								style="width: 350px; font-family: verdana, monospace;"
								type="text" class="form-control" maxlength="9" id="telefon"
								placeholder="Podaj nowy TELEFON" name="telefon"><span
								class="komunikat"></span>
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Zmie�
								e-mail</label> <input
								style="width: 350px; font-family: verdana, monospace;"
								type="email" class="form-control" id="email"
								placeholder="Podaj nowy E-MAIL" name="email">
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Zmie�
								oddzia�</label> <select class="form-control"
								style="width: 350px; font-family: verdana, monospace;"
								name="id_oddzialu">
								<option value="0">Bez zmian</option>
								<option value="101">101 - Oddzia� Neurologiczny</option>
								<option value="102">102 - Oddzia� Noworodkowy</option>
								<option value="103">103 - Oddzia� Ginekologiczno -
									Po�o�niczy</option>
								<option value="104">104 - Oddzia� Rehabilitacji</option>
								<option value="105">105 - Oddzia� Kardiologii</option>
								<option value="106">106 - Szpitalny Oddzia� Ratunkowy</option>
								<option value="107">107 - Oddzia� Urazowo-Ortopedyczny</option>
								<option value="108">108 - Oddzia� Chor�b Wewn�trznych</option>
								<option value="109">109 - Oddzia� Kardiochirurgii</option>
								<option value="110">110 - Oddzia� Transplantologii</option>
								<option value="111">111 - Oddzia� Chirurgii Og�lnej i
									Ma�oinwazyjnej</option>
								<option value="112">112 - Oddzia� Intensywnej Terapii i
									Anestezjologii</option>
								<option value="113">113 - Blok Operacyjny</option>
							</select>
						</div>
						<input type="hidden" class="form-control" name="progress"
							value="edit">
						<button type="submit" class="btn btn-primary" value="Edytuj">
							<i class="fa fa-pencil"></i>&nbsp; Zapisz
						</button>
					</form>
				</div>
				<%
					List<Uzytkownik> users = (List<Uzytkownik>) request.getAttribute("userList");
					List<Oddzial> branch = (List<Oddzial>) request.getAttribute("branchList");
				%>

				<div class="col-lg-4"
					style="padding-top: 10px; padding-bottom: 10px; padding-left: 20px; background-color: #003366;">
					<div class="row">
						<div class="col-lg-12"
							style="font-size: 15px; color: #99CCFF; font-family: verdana, monospace; font-weight: bold;">Zmieniasz
							dane u�ytkownika:</div>
					</div>
					<div class="row">
						<div class="col-lg-4"
							style="font-size: 13px; color: #003366; font-family: verdana, monospace;">--</div>
						<div class="col-lg-8"
							style="font-size: 13px; color: #003366; font-family: verdana, monospace;">--</div>
					</div>
					<div class="row">
						<div class="col-lg-4"
							style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-weight: bold;">Login:</div>
						<div class="col-lg-8"
							style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-style: italic;"><%=request.getParameter("login")%></div>
					</div>
					<div class="row">
						<div class="col-lg-4"
							style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-weight: bold;">Imi�:</div>
						<div class="col-lg-8"
							style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-style: italic;">
							<%
								for (Uzytkownik uzytkownik : users) {
									if (uzytkownik.getLogin().equals(request.getParameter("login"))) {
							%><%=uzytkownik.getImie()%>

							<%
								}
								}
							%>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-4"
							style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-weight: bold;">Nazwisko:</div>
						<div class="col-lg-8"
							style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-style: italic;">
							<%
								for (Uzytkownik uzytkownik : users) {
									if (uzytkownik.getLogin().equals(request.getParameter("login"))) {
							%><%=uzytkownik.getNazwisko()%>

							<%
								}
								}
							%>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-4"
							style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-weight: bold;">Telefon:</div>
						<div class="col-lg-8"
							style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-style: italic;">
							<%
								for (Uzytkownik uzytkownik : users) {
									if (uzytkownik.getLogin().equals(request.getParameter("login"))) {
							%><%=uzytkownik.getTelefon()%>

							<%
								}
								}
							%>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-4"
							style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-weight: bold;">E-mail:</div>
						<div class="col-lg-8"
							style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-style: italic;">
							<%
								for (Uzytkownik uzytkownik : users) {
									if (uzytkownik.getLogin().equals(request.getParameter("login"))) {
							%><%=uzytkownik.getEmail()%>

							<%
								}
								}
							%>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-4"
							style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-weight: bold;">PESEL:</div>
						<div class="col-lg-8"
							style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-style: italic;">
							<%
								for (Uzytkownik uzytkownik : users) {
									if (uzytkownik.getLogin().equals(request.getParameter("login"))) {
							%><%=uzytkownik.getPesel()%>

							<%
								}
								}
							%>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-4"
							style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-weight: bold;">Prawo
							dost�pu:</div>
						<div class="col-lg-8"
							style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-style: italic;">
							<%
								for (Uzytkownik uzytkownik : users) {
									if (uzytkownik.getLogin().equals(request.getParameter("login"))) {
							%><%=uzytkownik.getPrawoDostepu()%>

							<%
								}
								}
							%>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-4"
							style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-weight: bold;">Oddzia�:</div>
						<div class="col-lg-8"
							style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-style: italic;">
							<%
								for (Uzytkownik uzytkownik : users) {
									for (Oddzial oddzial : branch) {
										if (uzytkownik.getLogin().equals(request.getParameter("login"))) {
											if (uzytkownik.getID_oddzialu_FK() == (oddzial.getID_oddzialu())) {
							%><%=oddzial.getNazwa()%>

							<%
								}
										}
									}
								}
							%>
						</div>
					</div>

				</div>


			</div>
		</div>
</body>
</html>