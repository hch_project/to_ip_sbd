package com.hch.equimed.dao;

import java.util.List;
import com.hch.equimed.model.*;

public interface EquimedDAO {

	public boolean addEquip(int nr_id, String nazwa, String producent, int nr_in, String data_zakupu, String dw,
			String gwarancja, String ostatni_przeglad, int id_oddzialu);
	
	public int nextID();

	public List<Sprzet> findAllEquipment();

	public List<Sprzet> searchEquipmentByKeyword(String keyWord);

	public void deleteEquipment(String nazwa);

	public List<Oddzial> findAllBranch();

	public boolean saveLoan(long id_wyp, int id_oddzialu_w, int id_oddzialu_p, String data_p, String data_k,
			long nr_id);

	public List<Wypozyczenie> findLoanEquipment();

	public void deleteLoanEquipment(int id_wyp);

	public boolean saveRepair(int nr_sr, String opis, String data, int nr_id);

}
