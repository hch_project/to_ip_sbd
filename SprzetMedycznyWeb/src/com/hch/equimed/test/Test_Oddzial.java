package com.hch.equimed.test;

import static org.junit.Assert.*;
import org.junit.Test;

import com.hch.equimed.model.Oddzial;

public class Test_Oddzial extends Oddzial{

	@Test
	public void testSetID_oddzialu() {
		setID_oddzialu(101);
		assertEquals(101, getID_oddzialu());
	}

	@Test
	public void testSetNazwa() {
		setNazwa("Kardiologia");
		assertEquals("Kardiologia", getNazwa());
	}

}
