package com.hch.equimed.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.hch.equimed.model.*;

public class EquimedDAOImpl implements EquimedDAO {

	static int maxID;
	
	@Override
	public boolean addEquip(int nr_id, String nazwa, String producent, int nr_in, String data_zakupu, String dw,
			String gwarancja, String ostatni_przeglad, int id_oddzialu) {
		Connection connection = null;
		try {
			connection = DBOracle.connect();
			PreparedStatement statement = connection.prepareStatement(
					"INSERT INTO sprzet (nr_id, nazwa, producent, nr_in, data_zakupu, dw, gwarancja, ostatni_przeglad, id_oddzialu,awaria,wypozyczenie) values (?, ?, ?, ?, to_date(?,'yyyy-mm-dd'), to_date(?,'yyyy-mm-dd'), to_date(?,'yyyy-mm-dd'), to_date(?,'yyyy-mm-dd'), ?, 0, 0)");
			statement.setInt(1, nr_id);
			statement.setString(2, nazwa);
			statement.setString(3, producent);
			statement.setInt(4, nr_in);
			statement.setString(5, data_zakupu);
			statement.setString(6, dw);
			statement.setString(7, gwarancja);
			statement.setString(8, ostatni_przeglad);
			statement.setInt(9, id_oddzialu);
			statement.execute();
		} catch (SQLException e) {
			System.err.println("Blad przy wstawianiu sprzetu");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public List<Sprzet> findAllEquipment() {
		List<Sprzet> result = new ArrayList<>();

		String sql = "select * from Sprzet order by nr_id ASC";

		Connection connection = null;

		try {
			connection = DBOracle.connect();
			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Sprzet sprzet = new Sprzet();
				sprzet.setNr_ID(resultSet.getLong("nr_id"));
				sprzet.setNazwa(resultSet.getString("nazwa"));
				sprzet.setProducent(resultSet.getString("producent"));
				sprzet.setNr_in(resultSet.getInt("nr_in"));
				sprzet.setDataZakupu(resultSet.getString("data_zakupu"));
				sprzet.setDw(resultSet.getString("dw"));
				sprzet.setGwarancja(resultSet.getString("gwarancja"));
				sprzet.setOstatniPrzeglad(resultSet.getString("ostatni_przeglad"));
				sprzet.setID_oddzialu(resultSet.getInt("id_oddzialu"));
				sprzet.setAwaria(resultSet.getInt("awaria"));
				sprzet.setWypozyczenie(resultSet.getInt("wypozyczenie"));
				result.add(sprzet);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			DBOracle.closeConnection(connection);
		}
		return result;
	}

	@Override
	public List<Sprzet> searchEquipmentByKeyword(String keyWord) {
		List<Sprzet> result = new ArrayList<>();

		String sql = "select * from Sprzet s join Oddzial o on s.id_oddzialu=o.id_oddzialu where " + "s.nazwa like '%"
				+ keyWord.trim() + "%' or lower(s.nazwa) like '%" + keyWord.trim() + "%' or upper(s.nazwa) like '%"
				+ keyWord.trim() + "%'" + " or producent like '%" + keyWord.trim() + "%' or lower(producent) like '%"
				+ keyWord.trim() + "%' or upper(producent) like '%" + keyWord.trim() + "%'" + " or s.nr_id like '%"
				+ keyWord.trim() + "%'" + " or o.nazwa like '%" + keyWord.trim() + "%' or lower(o.nazwa) like '%"
				+ keyWord.trim() + "%' or upper(o.nazwa) like '%" + keyWord.trim() + "%'";

		Connection connection = null;

		try {
			connection = DBOracle.connect();
			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Sprzet sprzet = new Sprzet();
				sprzet.setNr_ID(resultSet.getLong("nr_id"));
				sprzet.setNazwa(resultSet.getString("nazwa"));
				sprzet.setProducent(resultSet.getString("producent"));
				sprzet.setNr_in(resultSet.getInt("nr_in"));
				sprzet.setDataZakupu(resultSet.getString("data_zakupu"));
				sprzet.setDw(resultSet.getString("dw"));
				sprzet.setGwarancja(resultSet.getString("gwarancja"));
				sprzet.setOstatniPrzeglad(resultSet.getString("ostatni_przeglad"));
				sprzet.setID_oddzialu(resultSet.getInt("id_oddzialu"));
				result.add(sprzet);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			DBOracle.closeConnection(connection);
		}
		return result;
	}

	@Override
	public void deleteEquipment(String nazwa) {

		String sql = "delete from sprzet where nazwa='" + nazwa + "'";

		DBOracle.connectOracle(sql);
	}

	@Override
	public List<Oddzial> findAllBranch() {
		List<Oddzial> result = new ArrayList<>();

		String sql = "select * from oddzial";

		Connection connection = null;

		try {
			connection = DBOracle.connect();
			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Oddzial oddzial = new Oddzial();
				oddzial.setID_oddzialu(resultSet.getInt("id_oddzialu"));
				oddzial.setNazwa(resultSet.getString("nazwa"));
				result.add(oddzial);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			DBOracle.closeConnection(connection);
		}
		return result;
	}

	@Override
	public boolean saveLoan(long id_wyp, int id_oddzialu_w, int id_oddzialu_p, String data_p, String data_k,
			long nr_id) {
		Connection connection = null;
		try {
			connection = DBOracle.connect();
			PreparedStatement statement = connection.prepareStatement(
					"INSERT INTO wypozyczenie (id_wyp, id_oddzialu_w, id_oddzialu_p, data_p, data_k, nr_id) values (?, ?, ?, to_date(?,'yyyy-mm-dd'), to_date(?,'yyyy-mm-dd'), ?)");
			statement.setLong(1, id_wyp);
			statement.setInt(2, id_oddzialu_w);
			statement.setInt(3, id_oddzialu_p);
			statement.setString(4, data_p);
			statement.setString(5, data_k);
			statement.setLong(6, nr_id);
			statement.execute();
		} catch (SQLException e) {
			System.err.println("Blad przy wprowadzaniu wypozyczeniu");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public List<Wypozyczenie> findLoanEquipment() {
		List<Wypozyczenie> result = new ArrayList<>();

		String sql = "select * from wypozyczenie";

		Connection connection = null;

		try {
			connection = DBOracle.connect();
			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Wypozyczenie wypozyczenie = new Wypozyczenie();
				wypozyczenie.setID_wyp(resultSet.getInt("id_wyp"));
				wypozyczenie.setID_oddzialu_w(resultSet.getInt("id_oddzialu_w"));
				wypozyczenie.setID_oddzialu_p(resultSet.getInt("id_oddzialu_p"));
				wypozyczenie.setData_p(resultSet.getString("data_p"));
				wypozyczenie.setData_k(resultSet.getString("data_k"));
				wypozyczenie.setNr_ID(resultSet.getInt("nr_id"));
				result.add(wypozyczenie);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			DBOracle.closeConnection(connection);
		}
		return result;
	}

	@Override
	public void deleteLoanEquipment(int nr_id) {

		String sql = "delete from wypozyczenie where nr_id='" + nr_id + "'";
		String sql1 = "update sprzet set wypozyczenie='0' where nr_id='" + nr_id + "'";

		DBOracle.connectOracle(sql);
		DBOracle.connectOracle(sql1);
	}

	@Override
	public boolean saveRepair(int nr_sr, String opis, String data, int nr_id) {
		Connection connection = null;
		try {
			connection = DBOracle.connect();
			PreparedStatement statement = connection.prepareStatement(
					"INSERT INTO naprawa (nr_sr, opis, data, nr_id) values (?, ?, to_date(?,'yyyy-mm-dd'), ?)");
			statement.setLong(1, nr_sr);
			statement.setString(2, opis);
			statement.setString(3, data);
			statement.setInt(4, nr_id);
			statement.execute();
		} catch (SQLException e) {
			System.err.println("Blad przy wprowadzaniu naprawy");
			e.printStackTrace();
			return false;
		}

		String sql = "update sprzet set awaria='0' where nr_id='" + nr_id + "'";
		DBOracle.connectOracle(sql);
		return true;
	}

	@Override
	public int nextID() {
		String sql = "select max(nr_id) from sprzet";

		Connection connection = null;

		try {
			connection = DBOracle.connect();
			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet resultSet = statement.executeQuery();
			resultSet.next();
			maxID=Integer.parseInt(resultSet.getObject(1).toString());
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			DBOracle.closeConnection(connection);
		}
		return maxID;
	}

}
