package com.hch.equimed.controller;

import com.hch.equimed.dao.*;
import com.hch.equimed.model.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpUtils;

public class UserController extends HttpServlet {

	private static UserDAO userDAO = new UserDAOImpl();
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserController() {
		super();
	}

	public void init(ServletConfig config) throws ServletException {
		super.init(config);

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String base = "/jsp/";
		String url = "logIn.jsp";
		String action = request.getParameter("action");
		String login = request.getParameter("login");
		String haslo = request.getParameter("haslo");
		String imie = request.getParameter("imie");
		String nazwisko = request.getParameter("nazwisko");
		String telefon = request.getParameter("telefon");
		String email = request.getParameter("email");
		String pesel = request.getParameter("pesel");
		String prawoDostepu = request.getParameter("prawoDostepu");
		String id_oddzialu = request.getParameter("id_oddzialu");

		if (action != null) {
			switch (action) {
			case "allUsers":
				findAllUser(request, response);
				findAllBranch(request, response);
				url = base + "listOfUsers.jsp";
				break;
			case "addUser":
				findAllUser(request, response);
				findAllBranch(request, response);
				url = base + "addUser.jsp";
				break;
			case "add":
				userDAO.addUser(login, imie, nazwisko, telefon, email, Integer.parseInt(id_oddzialu), pesel,
						prawoDostepu);
				userDAO.addPassword(login, haslo);
				findAllUser(request, response);
				findAllBranch(request, response);
				url = base + "listOfUsers.jsp";
				break;
			case "delete":
				userDAO.deleteUser(login);
				findAllUser(request, response);
				findAllBranch(request, response);
				url = base + "listOfUsers.jsp";
				break;
			case "logOut":
				url = "logIn.jsp";
				break;
			case "logOn":
				findAllUser(request, response);
				findAllBranch(request, response);
				url = base + "home.jsp";
				break;
			}
		}
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(url);
		requestDispatcher.forward(request, response);
	}

	private void findAllUser(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			UserDAO userDAO = new UserDAOImpl();
			List<Uzytkownik> userList = userDAO.findAllUser();
			request.setAttribute("userList", userList);

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	private void findAllBranch(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			EquimedDAO equimedDao = new EquimedDAOImpl();
			List<Oddzial> branchList = equimedDao.findAllBranch();
			request.setAttribute("branchList", branchList);

		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
