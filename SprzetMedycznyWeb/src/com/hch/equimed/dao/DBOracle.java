package com.hch.equimed.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBOracle {

	static public Connection connect() throws SQLException {

		String driver = "oracle.jdbc.driver.OracleDriver";
		String url = "jdbc:oracle:thin:@localhost:1521:SPRZETMEDYCZ";
		String user = "SprzetMedyczny";
		String password = "SprzetMedyczny";

		try {
			Class.forName(driver);

			// Connection connect = DriverManager.getConnection(url, user,
			// password);

		} catch (Exception ex) {

			ex.printStackTrace();
			System.out.println("B��d");
		}
		return DriverManager.getConnection(url, user, password);

	}

	static public void closeConnection(Connection connection) {
		if (connection == null)
			return;
		try {
			connection.close();
		} catch (Exception ex) {

		}
	}

	static public void connectOracle(String sql) {
		Connection connection = null;
		try {
			connection = connect();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.executeUpdate();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			closeConnection(connection);
		}
	}
}
