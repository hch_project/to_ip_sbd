package com.hch.equimed.controller;

import com.hch.equimed.dao.*;
import com.hch.equimed.model.*;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EditController extends HttpServlet {

	private static EditDAO editDao = new EditDAOImpl();
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EditController() {
		super();
	}

	public void init(ServletConfig config) throws ServletException {
		super.init(config);

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String base = "/jsp/";
		// String url = "login.html";
		String url = "logIn.jsp";
		String action = request.getParameter("action");
		String login = request.getParameter("login");
		String haslo = request.getParameter("haslo");
		String nazwisko = request.getParameter("nazwisko");
		String telefon = request.getParameter("telefon");
		String email = request.getParameter("email");
		String id_oddzialu = request.getParameter("id_oddzialu");
		String nr_id = request.getParameter("nr_id");
		String nr_in = request.getParameter("nr_in");
		String ostatni_przeglad = request.getParameter("ostatni_przeglad");

		if (action != null) {
			switch (action) {
			// edycja ADMIN
			case "editUser":
				findAllUser(request, response);
				findAllBranch(request, response);
				url = base + "editUser.jsp";
				break;
			case "edit":
				editDao.updateUser(login, haslo, nazwisko, telefon, email, Integer.parseInt(id_oddzialu));
				findAllUser(request, response);
				findAllBranch(request, response);
				url = base + "listOfUsers.jsp";
				break;
			// edycja LEKARZ
			case "damage_L":
				editDao.changeStatus(Integer.parseInt(nr_id), 1, 0);
				findAllEquipment(request, response);
				findAllBranch(request, response);
				url = base + "listOfEquip_lp.jsp";
				break;
			// edycja SERWIS
			case "editDate":
				findAllEquipment(request, response);
				findAllBranch(request, response);
				url = base + "editDate.jsp";
				break;
			case "changeDate":
				editDao.changeDate(Integer.parseInt(nr_id), ostatni_przeglad);
				findAllEquipment(request, response);
				findAllBranch(request, response);
				url = base + "listOfEquip_serwis.jsp";
				break;
			// edycja INWENTARYZACJA
			case "editNrIn":
				findAllEquipment(request, response);
				findAllBranch(request, response);
				url = base + "editNrInj.jsp";
				break;
			case "changeNrIn":
				editDao.changeNrIn(Integer.parseInt(nr_id), Integer.parseInt(nr_in));
				findAllEquipment(request, response);
				findAllBranch(request, response);
				url = base + "listOfEquip_inw.jsp";
				break;
			case "damage_I":
				editDao.changeStatus(Integer.parseInt(nr_id), 1, 0);
				findAllEquipment(request, response);
				findAllBranch(request, response);
				url = base + "listOfEquip_inw.jsp";
				break;
			}
		}

		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(url);
		requestDispatcher.forward(request, response);
	}

	private void findAllUser(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			UserDAO userDAO = new UserDAOImpl();
			List<Uzytkownik> userList = userDAO.findAllUser();
			request.setAttribute("userList", userList);

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	private void findAllBranch(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			EquimedDAO equimedDao = new EquimedDAOImpl();
			List<Oddzial> branchList = equimedDao.findAllBranch();
			request.setAttribute("branchList", branchList);

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	//
	private void findAllEquipment(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			EquimedDAO equimedDao = new EquimedDAOImpl();
			List<Sprzet> equipmentList = equimedDao.findAllEquipment();
			request.setAttribute("equipmentList", equipmentList);

		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
