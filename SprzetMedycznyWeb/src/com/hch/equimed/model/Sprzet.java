package com.hch.equimed.model;

public class Sprzet {
	private long nr_ID;
	private String nazwa;
	private String producent;
	private int nr_in;
	private String dataZakupu;
	private String dw;
	private String gwarancja;
	private String ostatniPrzeglad;
	private int ID_oddzialu;
	private int awaria;
	private int wypozyczenie;

	public long getNr_ID() {
		return nr_ID;
	}

	public void setNr_ID(long nr_ID) {
		this.nr_ID = nr_ID;
	}

	public String getProducent() {
		return producent;
	}

	public void setProducent(String producent) {
		this.producent = producent;
	}

	public String getDataZakupu() {
		return dataZakupu;
	}

	public void setDataZakupu(String dataZakupu) {
		this.dataZakupu = dataZakupu;
	}

	public String getDw() {
		return dw;
	}

	public void setDw(String dw) {
		this.dw = dw;
	}

	public String getGwarancja() {
		return gwarancja;
	}

	public void setGwarancja(String gwarancja) {
		this.gwarancja = gwarancja;
	}

	public String getOstatniPrzeglad() {
		return ostatniPrzeglad;
	}

	public void setOstatniPrzeglad(String ostatniPrzeglad) {
		this.ostatniPrzeglad = ostatniPrzeglad;
	}

	public int getID_oddzialu() {
		return ID_oddzialu;
	}

	public void setID_oddzialu(int iD_oddzialu) {
		ID_oddzialu = iD_oddzialu;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public int getNr_in() {
		return nr_in;
	}

	public void setNr_in(int nr_in) {
		this.nr_in = nr_in;
	}

	public int getAwaria() {
		return awaria;
	}

	public void setAwaria(int awaria) {
		this.awaria = awaria;
	}

	public int getWypozyczenie() {
		return wypozyczenie;
	}

	public void setWypozyczenie(int wypozyczenie) {
		this.wypozyczenie = wypozyczenie;
	}

	public String toString() {
		return "ID: " + nr_ID + "\tNazwa: " + nazwa + "\tProducent: " + producent + "\tNumer inwentarzowy: " + nr_in
				+ "\tData zakupu: " + dataZakupu + "\tData wprowadzenia: " + dw + "\tGwarancja: " + gwarancja
				+ "\tOstatni przegl�d: " + ostatniPrzeglad + "\tID oddzialu: " + ID_oddzialu;
	}
}
