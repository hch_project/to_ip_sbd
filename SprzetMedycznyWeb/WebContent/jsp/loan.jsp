<%@ page language="java" contentType="text/html; charset=ISO-8859-2"
	pageEncoding="ISO-8859-2"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.hch.equimed.model.*"%>
<%@ page import="java.io.*,java.util.*, javax.servlet.*"%>
<%@page import="java.text.SimpleDateFormat"%>



<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<script src="js/jquery-1.9.1.js"></script>
<link rel="stylesheet" href="css/equi.css" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet">
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<script src="js/equi.js" type="text/javascript"></script>

</head>
<body>
	<div id="centered">

		<jsp:include page="header_lp.jsp" flush="true" />

		<div class="container">
			<div class="row">
				<div class="col-lg-4"></div>
				<div class="col-lg-4">
					<span class="label"
						style="font-size: 20px; background-color: #99CCFF; color: #003366;">Wypo�yczanie
						sprz�tu</span>
					<%
						List<Sprzet> equipment = (List<Sprzet>) request.getAttribute("equipmentList");
						List<Oddzial> branch = (List<Oddzial>) request.getAttribute("branchList");
					%>
				</div>
				<div class="col-lg-4"></div>
			</div>
			</br>
			<div class="row">
				<div class="col-lg-3"></div>
				<div class="col-lg-6"
					style="padding-top: 10px; padding-bottom: 10px; padding-left: 20px; background-color: #003366;">
					<form role="form">
						<input type="hidden" name="action" value="loan" />
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Numer
								ID</label> <input style="width: 400px; font-family: verdana, monospace;"
								readonly="readonly" type="text" class="form-control" id="nr_id"
								value="<%=request.getParameter("numerID")%>" name="nr_id">

						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Nazwa</label>
							<input style="width: 400px; font-family: verdana, monospace;"
								disabled="disabled" type="text" class="form-control" id="nazwa"
								value="<%for (Sprzet sprzet : equipment) {
				long id = Long.parseLong(request.getParameter("numerID"));
				if (sprzet.getNr_ID() == id) {%><%=sprzet.getNazwa()%>

							<%}
			}%>"
								name="nazwa">
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Data
								wypo�yczenia</label> <input
								style="width: 400px; font-family: verdana, monospace;"
								readonly="readonly" type="text" class="form-control" id="data_p"
								value="<%Date dNow = new Date();
			SimpleDateFormat ft = new SimpleDateFormat(" yyyy-MM-dd ");
			out.print(ft.format(dNow));%>
"
								name="data_p">
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Numer
								Wypo�yczenia</label> <input
								style="width: 400px; font-family: verdana, monospace;"
								type="text" class="form-control" id="nr_wyp"
								placeholder="Podaj NUMER WYPO�YCZENIA" name="id_wyp">
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Data
								zwrotu</label> <input
								style="width: 400px; font-family: verdana, monospace;"
								min="<%Date dNow11 = new Date();
			SimpleDateFormat ft11 = new SimpleDateFormat("yyyy-MM-dd");
			out.print(ft11.format(dNow11));%>"
								type="date" class="form-control" id="data_k"
								placeholder="Podaj DAT� ZWROTU" name="data_k">
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Oddzia�</label>
							<input style="width: 400px; font-family: verdana, monospace;"
								disabled="disabled" type="text" class="form-control"
								id="oddzial"
								value="<%for (Sprzet sprzet : equipment) {
				for (Oddzial oddzial : branch) {
					long id = Long.parseLong(request.getParameter("numerID"));
					if (sprzet.getNr_ID() == id) {
						if (sprzet.getID_oddzialu() == (oddzial.getID_oddzialu())) {%><%=oddzial.getNazwa()%>

							<%}
					}
				}
			}%>"
								name="id_oddzialu_w">

							<%
								for (Sprzet sprzet : equipment) {
									for (Oddzial oddzial : branch) {
										long id = Long.parseLong(request.getParameter("numerID"));
										if (sprzet.getNr_ID() == id) {
											if (sprzet.getID_oddzialu() == (oddzial.getID_oddzialu())) {
												long id_oddz = oddzial.getID_oddzialu();
							%>
							<input type="hidden" name="id_oddzialu_w" value="<%=id_oddz%>" />
							<%
								}
										}
									}
								}
							%>
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Oddzia�,
								kt�ry po�ycza</label> <select class="form-control"
								style="width: 400px; font-family: verdana, monospace;"
								name="id_oddzialu_p">
								<option value="101">101 - Oddzia� Neurologiczny</option>
								<option value="102">102 - Oddzia� Noworodkowy</option>
								<option value="103">103 - Oddzia� Ginekologiczno -
									Po�o�niczy</option>
								<option value="104">104 - Oddzia� Rehabilitacji</option>
								<option value="105">105 - Oddzia� Kardiologii</option>
								<option value="106">106 - Szpitalny Oddzia� Ratunkowy</option>
								<option value="107">107 - Oddzia� Urazowo-Ortopedyczny</option>
								<option value="108">108 - Oddzia� Chor�b Wewn�trznych</option>
								<option value="109">109 - Oddzia� Kardiochirurgii</option>
								<option value="110">110 - Oddzia� Transplantologii</option>
								<option value="111">111 - Oddzia� Chirurgii Og�lnej i
									Ma�oinwazyjnej</option>
								<option value="112">112 - Oddzia� Intensywnej Terapii i
									Anestezjologii</option>
								<option value="113">113 - Blok Operacyjny</option>
							</select>
						</div>
						<input type="hidden" class="form-control" name="progress"
							value="loan">
						<button type="submit" class="btn btn-primary" value="loan"><i class="fa fa-check"></i>&nbsp; Zatwierd�</button>
					</form>
				</div>
				<div class="col-lg-3"></div>
			</div>
		</div>
</body>
</html>