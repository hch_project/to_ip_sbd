c<%@ page language="java" contentType="text/html; charset=ISO-8859-2"
	pageEncoding="ISO-8859-2"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.hch.equimed.model.Sprzet"%>
<%@ page import="java.io.*,java.util.*, javax.servlet.*"%>
<%@page import="java.text.SimpleDateFormat"%>


<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<script src="js/jquery-1.9.1.js"></script>
<link rel="stylesheet" href="css/equi.css" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet">
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<script src="js/equi.js" type="text/javascript"></script>
</head>
<body>
	<div id="centered">

		<jsp:include page="header_inw.jsp" flush="true" />

		<div class="container">
			<div class="row">
				<div class="col-lg-4"></div>
				<div class="col-lg-4">
					<span class="label"
						style="font-size: 20px; background-color: #99CCFF; color: #003366;">Dodawanie
						sprz�tu</span>
				</div>
				<div class="col-lg-4"></div>
			</div>
			</br>
			<div class="row">
				<div class="col-lg-3"></div>
				<div class="col-lg-6"
					style="padding-top: 10px; padding-bottom: 10px; padding-left: 20px; background-color: #003366;">
					<form role="form">
						<%
						int maxID = (Integer) request.getAttribute("maxID");
						%>
						<input type="hidden" name="action" value="addEquip" />
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Numer
								ID</label> <input style="width: 400px; font-family: verdana, monospace;"
								type="text" class="form-control" id="nr_id"
								value="<%=maxID+1 %>" name="nr_id" readonly="readonly">
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Nazwa</label>
							<input style="width: 400px; font-family: verdana, monospace;"
								type="text" class="form-control" id="nazwa"
								placeholder="Podaj NAZW�" name="nazwa" required="required">
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Producent</label>
							<input style="width: 400px; font-family: verdana, monospace;"
								type="author" class="form-control" id="producent"
								placeholder="Podaj PRODUCENTA" name="producent" required="required">
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Numer
								inwentarzowy</label> <input
								style="width: 400px; font-family: verdana, monospace;"
								type="text" class="form-control" id="nr_in"
								placeholder="Podaj numer INWENTARZOWY" name="nr_in" required="required">
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Data
								zakupu</label> <input
								style="width: 400px; font-family: verdana, monospace;"
								max="<%Date dNow_zakup = new Date();
			SimpleDateFormat ft_zakup = new SimpleDateFormat("yyyy-MM-dd");
			out.print(ft_zakup.format(dNow_zakup));%>"
								type="date" class="form-control" id="data_zakupu"
								placeholder="Podaj DAT� ZAKUPU" name="data_zakupu" required="required">
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Data
								wprowadzenia</label> <input
								style="width: 400px; font-family: verdana, monospace;"
								min="<%Date dNow_dw = new Date();
			SimpleDateFormat ft_dw = new SimpleDateFormat("yyyy-MM-dd");
			out.print(ft_dw.format(dNow_dw));%>"
								type="date" class="form-control" id="dw"
								placeholder="Podaj DAT� WPROWADZENIA" name="dw" required="required">
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Data
								gwarancji</label> <input
								style="width: 400px; font-family: verdana, monospace;"
								min="<%Date dNow_gwarancja = new Date();
			SimpleDateFormat ft_gwarancja = new SimpleDateFormat("yyyy-MM-dd");
			out.print(ft_gwarancja.format(dNow_gwarancja));%>"
								type="date" class="form-control" id="gwarancja"
								placeholder="Podaj DAT� GWARANCJI" name="gwarancja" required="required">
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Data
								przegl�du</label> <input
								style="width: 400px; font-family: verdana, monospace;"
								min="<%Date dNow_przeglad = new Date();
			SimpleDateFormat ft_przeglad = new SimpleDateFormat("yyyy-MM-dd");
			out.print(ft_przeglad.format(dNow_przeglad));%>"
								type="date" class="form-control" id="data_przegladu"
								placeholder="Podaj DAT� PRZEGL�DU" name="ostatni_przeglad" required="required">
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Oddzia�</label>
							<select class="form-control"
								style="width: 400px; font-family: verdana, monospace;"
								name="id_oddzialu">
								<option value="101">101 - Oddzia� Neurologiczny</option>
								<option value="102">102 - Oddzia� Noworodkowy</option>
								<option value="103">103 - Oddzia� Ginekologiczno -
									Po�o�niczy</option>
								<option value="104">104 - Oddzia� Rehabilitacji</option>
								<option value="105">105 - Oddzia� Kardiologii</option>
								<option value="106">106 - Szpitalny Oddzia� Ratunkowy</option>
								<option value="107">107 - Oddzia� Urazowo-Ortopedyczny</option>
								<option value="108">108 - Oddzia� Chor�b Wewn�trznych</option>
								<option value="109">109 - Oddzia� Kardiochirurgii</option>
								<option value="110">110 - Oddzia� Transplantologii</option>
								<option value="111">111 - Oddzia� Chirurgii Og�lnej i
									Ma�oinwazyjnej</option>
								<option value="112">112 - Oddzia� Intensywnej Terapii i
									Anestezjologii</option>
								<option value="113">113 - Blok Operacyjny</option>
								<option value="114">114 - Administracja</option>
								<option value="115">115 - Aparatura medyczna</option>
							</select>
						</div>
						<input type="hidden" class="form-control" name="progress"
							value="add">
						<button type="submit" class="btn btn-primary" value="Dodaj">
							<i class="fa fa-plus"></i>&nbsp; Dodaj
						</button>
					</form>
				</div>
				<div class="col-lg-3"></div>
			</div>
		</div>
</body>
</html>