<%@page import="sun.invoke.empty.Empty"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-2"
	pageEncoding="ISO-8859-2"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.hch.equimed.model.*"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page import="java.io.*,java.util.*, javax.servlet.*"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>

<%
	SimpleDateFormat simpleDateHere = new SimpleDateFormat("yyyy-MM-dd");
%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<script src="js/jquery-1.9.1.js"></script>
<link rel="stylesheet" href="css/equi.css" type="text/css" />
<script src="js/equi.js" type="text/javascript"></script>
</head>
<body>
	<div id="centered">

		<jsp:include page="header_serwis.jsp" flush="true" />

		<div class="container">
			<div class="row">
				<div class="col-lg-4"></div>
				<div class="col-lg-4">
					<span class="label"
						style="font-size: 20px; background-color: #99CCFF; color: #003366;">Edycja
						daty przegl�du</span>
				</div>
				<div class="col-lg-4"></div>
			</div>
			</br>
			<div class="row">
				<div class="col-lg-4"></div>
				<div class="col-lg-4">
					<span class="label"
						style="font-size: 15px; background-color: #99CCFF; color: #003366;">UWAGA!
						Data zmienia si� na dzisiejsz�<span>
				</div>
				<div class="col-lg-4"></div>
			</div>
			</br>
			<%
				List<Sprzet> equipment = (List<Sprzet>) request.getAttribute("equipmentList");
				List<Oddzial> branch = (List<Oddzial>) request.getAttribute("branchList");
			%>
			<input type="hidden" name="action" value="changeDate" /> <input
				type="hidden" name="nr_id"
				value="<%=request.getParameter("numerID")%>" />
			<div class="row">
				<div class="col-lg-3"></div>
				<div class="col-lg-6"
					style="padding-top: 10px; padding-bottom: 10px; padding-left: 20px; background-color: #003366;">
					<form role="form">
						<input type="hidden" name="action" value="changeDate" />
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Numer
								ID</label> <input style="width: 400px; font-family: verdana, monospace;"
								readonly="readonly" type="text" class="form-control" id="nr_id"
								value="<%=request.getParameter("numerID")%>" name="nr_id">

						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Nazwa</label>
							<input style="width: 400px; font-family: verdana, monospace;"
								readonly="readonly" type="text" class="form-control" id="nazwa"
								value="<%for (Sprzet sprzet : equipment) {
				long id = Long.parseLong(request.getParameter("numerID"));
				if (sprzet.getNr_ID() == id) {%><%=sprzet.getNazwa()%>

							<%}
			}%>"
								name="nazwa">
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Producent</label>
							<input style="width: 400px; font-family: verdana, monospace;"
								readonly="readonly" type="text" class="form-control"
								id="producent"
								value="<%for (Sprzet sprzet : equipment) {
				long id = Long.parseLong(request.getParameter("numerID"));
				if (sprzet.getNr_ID() == id) {%><%=sprzet.getProducent()%>
							<%}
			}%>"
								name="producent">
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Numer
								inwentaryzacyjny</label> <input
								style="width: 400px; font-family: verdana, monospace;"
								readonly="readonly" type="text" class="form-control" id="nr_in"
								value="<%for (Sprzet sprzet : equipment) {
				long id = Long.parseLong(request.getParameter("numerID"));
				if (sprzet.getNr_ID() == id) {%><%=sprzet.getNr_in()%>

							<%}
			}%>"
								name="nr_in">
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Data
								zakupu</label> <input
								style="width: 400px; font-family: verdana, monospace;"
								readonly="readonly" type="text" class="form-control"
								id="data_zakupu"
								value="<%for (Sprzet sprzet : equipment) {
				long id = Long.parseLong(request.getParameter("numerID"));
				if (sprzet.getNr_ID() == id) {Date date_zakupu = simpleDateHere.parse(sprzet.getDataZakupu());%><%=simpleDateHere.format(date_zakupu)%>
							<%}
			}%>
			"
								name="data_zakupu">
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Data
								wprowadzenia</label> <input
								style="width: 400px; font-family: verdana, monospace;"
								readonly="readonly" type="text" class="form-control" id="dw"
								value="<%for (Sprzet sprzet : equipment) {
				long id = Long.parseLong(request.getParameter("numerID"));
				if (sprzet.getNr_ID() == id) {Date dw = simpleDateHere.parse(sprzet.getDw());%><%=simpleDateHere.format(dw)%>
							<%}
			}%>
			"
								name="dw">
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Gwarancja</label>
							<input style="width: 400px; font-family: verdana, monospace;"
								readonly="readonly" type="text" class="form-control"
								id="gwarancja"
								value="<%for (Sprzet sprzet : equipment) {
				long id = Long.parseLong(request.getParameter("numerID"));
				if (sprzet.getNr_ID() == id) {Date gwarancja = simpleDateHere.parse(sprzet.getGwarancja());%><%=simpleDateHere.format(gwarancja)%>
							<%}
			}%>
			"
								name="gwarancja">
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Ostatni
								przegl�d ZMIANA NA DAT� DZISIEJSZ�</label> <input
								style="width: 400px; font-family: verdana, monospace;"
								readonly="readonly" type="text" class="form-control"
								id="ostatni_przeglad"
								value="<%Date dNow = new Date();
			SimpleDateFormat ft = new SimpleDateFormat(" yyyy-MM-dd ");
			out.print(ft.format(dNow));%>
			"
								name="ostatni_przeglad">
						</div>
						<div class="form-group">
							<label
								style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace;">Oddzia�</label>
							<input style="width: 400px; font-family: verdana, monospace;"
								disabled="disabled" type="text" class="form-control"
								id="oddzial"
								value="<%for (Sprzet sprzet : equipment) {
				for (Oddzial oddzial : branch) {
					long id = Long.parseLong(request.getParameter("numerID"));
					if (sprzet.getNr_ID() == id) {
						if (sprzet.getID_oddzialu() == (oddzial.getID_oddzialu())) {%><%=oddzial.getNazwa()%>

							<%}
					}
				}
			}%>"
								name="id_oddzialu">

							<%
								for (Sprzet sprzet : equipment) {
									for (Oddzial oddzial : branch) {
										long id = Long.parseLong(request.getParameter("numerID"));
										if (sprzet.getNr_ID() == id) {
											if (sprzet.getID_oddzialu() == (oddzial.getID_oddzialu())) {
												long id_oddz = oddzial.getID_oddzialu();
							%>
							<input type="hidden" name="id_oddzialu" value="<%=id_oddz%>" />
							<%
								}
										}
									}
								}
							%>
						</div>
						<input type="hidden" class="form-control" name="progress"
							value="change">
						<button type="submit" class="btn btn-primary" value="Edytuj"
							onclick="window.history.pushState('Sprzet', 'Sprzet', 'http://localhost:8080/SprzetMedycznyWeb/edit')">
							<i class="fa fa-pencil"></i>&nbsp; Zapisz
						</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>