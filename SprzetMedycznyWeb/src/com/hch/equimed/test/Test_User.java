/**
 * 
 */
package com.hch.equimed.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.hch.equimed.dao.UserDAOImpl;
import com.hch.equimed.model.Sprzet;
import com.hch.equimed.model.Uzytkownik;

/**
 * @author Doubler
 *
 */
public class Test_User extends UserDAOImpl {

	String login="Ltest";
	String haslo="test_nazwisko";
	String bledneHaslo="haslo";
	String imie="test_imie";
	String nazwisko="test_nazwisko";
	String telefon="456154";
	String email="Ltest@test.pl";
	String pesel="15421645834";
	String prawoDostepu="Admin";
	int id_oddzialu=101;
	

	/**
	 * Test method for {@link com.hch.equimed.dao.UserDAOImpl#findAllUser()}.
	 */
	@Test
	public void testFindAllUser() {
		List<Uzytkownik> uzytkownicy = findAllUser();
		
		for(Uzytkownik uzytkownik : uzytkownicy){
				assertTrue(!uzytkownik.getLogin().isEmpty());
				assertFalse(uzytkownik.getLogin().isEmpty());
		}
	}
	/**
	 * Test method for {@link com.hch.equimed.dao.UserDAOImpl#addUser(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testAddUser() {
		assertTrue(addUser(login, imie, nazwisko, telefon, email, id_oddzialu, pesel, prawoDostepu));
		assertFalse(addUser(login, imie, nazwisko, telefon, email, id_oddzialu, pesel, prawoDostepu));
	}
	
	/**
	 * Test method for {@link com.hch.equimed.dao.UserDAOImpl#addPassword(java.lang.String, java.lang.String,)}.
	 */
	@Test
	public void testAddPassword(){
		assertTrue(addPassword(login, haslo));
		assertFalse(addPassword(login, haslo));	
	}

	/**
	 * Test method for {@link com.hch.equimed.dao.UserDAOImpl#deleteUser(java.lang.String)}.
	 */
	@Test
	public void testDeleteUser() {
		deleteUser(login);
		List<Uzytkownik> uzytkownicy = findAllUser();
		
		for(Uzytkownik uzytkownik : uzytkownicy) {
			assertTrue(!uzytkownik.getLogin().contains(login));
			assertFalse(uzytkownik.getLogin().contains(login));
		}
	}
	/**
	 * Test method for {@link com.hch.equimed.dao.UserDAOImpl#checkUser(java.lang.String)}.
	 */
	@Test
	public void testCheckUser(){
		assertTrue(addUser(login, imie, nazwisko, telefon, email, id_oddzialu, pesel, prawoDostepu));
		assertTrue(checkUser(login, haslo));
		assertFalse(checkUser(login, bledneHaslo));
		deleteUser(login);
	}
}
