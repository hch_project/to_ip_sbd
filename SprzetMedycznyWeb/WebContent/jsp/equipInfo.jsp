<%@page import="oracle.net.aso.w"%>
<%@page import="sun.invoke.empty.Empty"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-2"
	pageEncoding="ISO-8859-2"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.hch.equimed.model.*"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>

<%
	String imageURL = application.getInitParameter("imageURL");
	SimpleDateFormat simpleDateHere = new SimpleDateFormat("yyyy-MM-dd");

	String user = (String) session.getAttribute("user");
	String userName = null;
	String sessionID = null;
	Cookie[] cookies = request.getCookies();
	if (cookies != null) {
		for (Cookie cookie : cookies) {
			if (cookie.getName().equals("user"))
				userName = cookie.getValue();
			if (cookie.getName().equals("JSESSIONID"))
				sessionID = cookie.getValue();
		}
	}
%>

<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<script src="js/jquery-1.9.1.js"></script>
<link rel="stylesheet" href="css/equi.css" type="text/css" />
<script src="js/equi.js" type="text/javascript"></script>
</head>
<body>
	<div id="centered">
		<%
			String access = userName.toUpperCase().substring(0, 1);
			switch (access) {
			case "A":
		%>
		<jsp:include page="header.jsp" flush="true" />
		<%
			break;
			case "S":
		%>
		<jsp:include page="header_serwis.jsp" flush="true" />
		<%
			break;
			case "L":
		%>
		<jsp:include page="header_lp.jsp" flush="true" />
		<%
			break;
			case "I":
		%>
		<jsp:include page="header_inw.jsp" flush="true" />
		<%
			break;
			}
		%>


		<div class="container" style="width: 1200px;">
			<div class="row">
				<div class="col-lg-4"></div>
				<div class="col-lg-4">
					<span class="label"
						style="font-size: 20px; background-color: #99CCFF; color: #003366;">Szczeg�owe
						informacje o sprz�cie</span>
				</div>
				<div class="col-lg-4"></div>
			</div>
			</br>
			<%
				List<Sprzet> brands = (List<Sprzet>) request.getAttribute("equipmentList");
				List<Oddzial> branch = (List<Oddzial>) request.getAttribute("branchList");
				List<Wypozyczenie> loanEquip = (List<Wypozyczenie>) request.getAttribute("equipmentLoanList");
				String nr_id = request.getParameter("numerID");
			%>

			<div class="col-lg-2"
				style="height: 85px; background: none; border: none;"></div>
			<div class="col-lg-5"
				style="padding-top: 10px; padding-bottom: 10px; padding-left: 20px; background-color: #003366; border: 1px solid #99CCFF;">
				<div class="row">
					<div class="col-lg-5"
						style="font-size: 13px; color: #003366; font-family: verdana, monospace;">--</div>
					<div class="col-lg-7"
						style="font-size: 13px; color: #003366; font-family: verdana, monospace;">--</div>
				</div>
				<div class="row">
					<div class="col-lg-5"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-style: italic;">Numer
						ID:</div>
					<div class="col-lg-7"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-weight: bold;"><%=request.getParameter("numerID")%></div>
				</div>
				</br>
				<div class="row">
					<div class="col-lg-5"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-style: italic;">Nazwa:</div>
					<div class="col-lg-7"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-weight: bold;">
						<%
							for (Sprzet sprzet : brands) {
								if (sprzet.getNr_ID() == Integer.parseInt(nr_id)) {
						%><%=sprzet.getNazwa()%>

						<%
							}
							}
						%>
					</div>
				</div>
				</br>
				<div class="row">
					<div class="col-lg-5"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-style: italic;">Producent:</div>
					<div class="col-lg-7"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-weight: bold;">
						<%
							for (Sprzet sprzet : brands) {
								if (sprzet.getNr_ID() == Integer.parseInt(nr_id)) {
						%><%=sprzet.getProducent()%>

						<%
							}
							}
						%>
					</div>
				</div>
				</br>
				<div class="row">
					<div class="col-lg-5"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-style: italic;">Numer
						inwentaryzacyjny:</div>
					<div class="col-lg-7"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-weight: bold;">
						<%
							for (Sprzet sprzet : brands) {
								if (sprzet.getNr_ID() == Integer.parseInt(nr_id)) {
						%><%=sprzet.getNr_in()%>

						<%
							}
							}
						%>
					</div>
				</div>
				</br>
				<div class="row">
					<div class="col-lg-5"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-style: italic;">Data
						zakupu:</div>
					<div class="col-lg-7"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-weight: bold;">
						<%
							for (Sprzet sprzet : brands) {
								if (sprzet.getNr_ID() == Integer.parseInt(nr_id)) {
						%>
						<%
							Date date_zakupu = simpleDateHere.parse(sprzet.getDataZakupu());
						%><%=simpleDateHere.format(date_zakupu)%>
						<%
							}
							}
						%>
					</div>
				</div>
				</br>
				<div class="row">
					<div class="col-lg-5"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-style: italic;">Data
						wprowadzenia:</div>
					<div class="col-lg-7"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-weight: bold;">
						<%
							for (Sprzet sprzet : brands) {
								if (sprzet.getNr_ID() == Integer.parseInt(nr_id)) {
						%>
						<%
							Date dw = simpleDateHere.parse(sprzet.getDw());
						%><%=simpleDateHere.format(dw)%>

						<%
							}
							}
						%>
					</div>
				</div>
				</br>
				<div class="row">
					<div class="col-lg-5"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-style: italic;">Gwarancja:</div>
					<div class="col-lg-7"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-weight: bold;">
						<%
							for (Sprzet sprzet : brands) {
								if (sprzet.getNr_ID() == Integer.parseInt(nr_id)) {
						%>
						<%
							Date gwarancja = simpleDateHere.parse(sprzet.getGwarancja());
						%><%=simpleDateHere.format(gwarancja)%>

						<%
							}
							}
						%>
					</div>
				</div>
				</br>
				<div class="row">
					<div class="col-lg-5"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-style: italic;">Ostatni
						przegl�d:</div>
					<div class="col-lg-7"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-weight: bold;">
						<%
							for (Sprzet sprzet : brands) {
								if (sprzet.getNr_ID() == Integer.parseInt(nr_id)) {
						%>
						<%
							Date ostatniprzeglad = simpleDateHere.parse(sprzet.getOstatniPrzeglad());
						%><%=simpleDateHere.format(ostatniprzeglad)%>

						<%
							}
							}
						%>
					</div>
				</div>
				</br>
				<div class="row">
					<div class="col-lg-5"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-style: italic;">Oddzia�:</div>
					<div class="col-lg-7"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-weight: bold;">
						<%
							for (Sprzet sprzet : brands) {
								for (Oddzial oddzial : branch) {
									if (sprzet.getNr_ID() == Integer.parseInt(nr_id)) {
										if (sprzet.getID_oddzialu() == (oddzial.getID_oddzialu())) {
						%><%=oddzial.getNazwa()%>

						<%
							}
									}
								}
							}
						%>
					</div>
				</div>
				</br>
				<div class="row">
					<div class="col-lg-5"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-style: italic;">Czy
						sprz�t jest w naprawie:</div>
					<div class="col-lg-7"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-weight: bold;">
						<%
							for (Sprzet sprzet : brands) {
								if (sprzet.getNr_ID() == Integer.parseInt(nr_id)) {
									if (sprzet.getAwaria() == 1) {
						%>TAK

						<%
							} else {
						%>NIE<%
							}
								}
							}
						%>
					</div>
				</div>
				</br>
				<div class="row">
					<div class="col-lg-5"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-style: italic;">Czy
						sprz�t jest wypo�yczony:</div>
					<div class="col-lg-7"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-weight: bold;">
						<%
							for (Sprzet sprzet : brands) {
								if (sprzet.getNr_ID() == Integer.parseInt(nr_id)) {
									if (sprzet.getWypozyczenie() == 1) {
						%>TAK

						<%
							} else {
						%>NIE<%
							}
								}
							}
						%>
					</div>
				</div>
				</br>
				<%
					for (Sprzet sprzet : brands) {
						if (sprzet.getNr_ID() == Integer.parseInt(nr_id)) {
							if (sprzet.getWypozyczenie() == 1) {
				%>
				<div class="row">
					<div class="col-lg-5"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-style: italic;">Oddzia�,
						na kt�ry jest wypo�yczony:</div>
					<div class="col-lg-7"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-weight: bold;">
						<%
							for (Oddzial oddzial : branch) {
											for (Wypozyczenie wyp : loanEquip) {
												if (wyp.getID_oddzialu_p() == (oddzial.getID_oddzialu())) {
						%><%=oddzial.getNazwa()%>

						<%
							}
											}
										}
						%>
					</div>
				</div>
				</br>
				<div class="row">
					<div class="col-lg-5"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-style: italic;">Wypo�yczony
						do dnia:</div>
					<div class="col-lg-7"
						style="font-size: 13px; color: #99CCFF; font-family: verdana, monospace; font-weight: bold;">
						<%
							for (Wypozyczenie wyp : loanEquip) {
						%>
						<%
							Date data_k = simpleDateHere.parse(wyp.getData_k());
						%><%=simpleDateHere.format(data_k)%>
						<%
							}
						%>
					</div>
				</div>
				<%
					}
						}
					}
				%>
			</div>
			<div class="col-lg-4"
				style="height: auto; background: #003366; border: 1px solid #99CCFF; padding: 10px 15px 30px 20px;">
				</br> <img style="width: 345px;"
					src="<%=imageURL%>/<%=request.getParameter("numerID")%>.jpg" />
			</div>
		</div>
	</div>
</body>
</html>