package com.hch.equimed.model;

public class Naprawa {
	private long nr_SR;
	private String opis;
	private String data;
	private long nr_ID;

	public long getNr_SR() {
		return nr_SR;
	}

	public void setNr_SR(long nr_SR) {
		this.nr_SR = nr_SR;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public long getNr_ID() {
		return nr_ID;
	}

	public void setNr_ID(long nr_ID) {
		this.nr_ID = nr_ID;
	}

}
