<%@ page language="java" contentType="text/html; charset=ISO-8859-2"
	pageEncoding="ISO-8859-2"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.hch.equimed.model.*"%>
<!DOCTYPE html >
<%
	String imageURL = application.getInitParameter("imageURL");
	String param3 = application.getInitParameter("param3");
%>
<%
	//allow access only if session exists
	String user = (String) session.getAttribute("user");
	String userName = null;
	String sessionID = null;
	Cookie[] cookies = request.getCookies();
	if (cookies != null) {
		for (Cookie cookie : cookies) {
			if (cookie.getName().equals("user"))
				userName = cookie.getValue();
			if (cookie.getName().equals("JSESSIONID"))
				sessionID = cookie.getValue();
		}
	}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<script src="js/jquery-1.9.1.js"></script>
<link rel="stylesheet" href="css/equi.css" type="text/css" />
<script src="js/equi.js" type="text/javascript"></script>
<link href="css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
	<div id="centered">

		<%
			String access = userName.toUpperCase().substring(0, 1);
			switch (access) {
			case "A":
		%>
		<jsp:include page="header.jsp" flush="true" />
		<%
			break;
			case "S":
		%>
		<jsp:include page="header_serwis.jsp" flush="true" />
		<%
			break;
			case "L":
		%>
		<jsp:include page="header_lp.jsp" flush="true" />
		<%
			break;
			case "I":
		%>
		<jsp:include page="header_inw.jsp" flush="true" />
		<%
			break;
			}
		%>

		<div class="container" style="width: 1200px;">
			<div class="row">
				<div class="col-lg-4"></div>
				<div class="col-lg-4">
					<span class="label"
						style="font-size: 20px; background-color: #99CCFF; color: #003366;">Lista
						znalezionego sprz�tu</span>
				</div>
				<div class="col-lg-4"></div>
			</div>
			</br>
			<div class="row">
				<div class="col-lg-2"
					style="height: 65px; background: none; border: none;"></div>
				<div class="col-lg-1"
					style="height: 65px; background-color: #99CCFF; color: #003366;">Numer
					ID</div>
				<div class="col-lg-2"
					style="height: 65px; background-color: #99CCFF; color: #003366;">Nazwa</div>
				<div class="col-lg-1"
					style="height: 65px; background-color: #99CCFF; color: #003366;">Producent</div>
				<div class="col-lg-2"
					style="height: 65px; background-color: #99CCFF; color: #003366;">Oddzia�</div>
				<div class="col-lg-2"
					style="height: 65px; background-color: #99CCFF; color: #003366;">Szczeg�y</div>
				<div class="col-lg-2" style="background: none; border: none;"></div>

			</div>

			<%
				String word = request.getParameter("keyWord");
				if (word.equals("Szukaj...")) {
			%>
			<div>
				</br>
				<div class="row">
					<div class="col-lg-4"></div>
					<div class="col-lg-4">
						<span class="label"
							style="font-size: 14px; background-color: #003366; color: #99CCFF; font-style: italic;">BRAK
							KRYTERIUM WYSZUKIWANIA</span>
					</div>
					<div class="col-lg-4"></div>
				</div>
			</div>
			<%
				} else {
			%>

			<div class="row">
				<%
					List<Oddzial> branch = (List<Oddzial>) request.getAttribute("branchList");
						List<Sprzet> brands = (List<Sprzet>) request.getAttribute("brandList");

						for (Sprzet sprzet : brands) {
				%>
				<div class="col-lg-2"
					style="height: 85px; background: none; border: none;"></div>
				<div class="col-lg-1" style="height: 85px;"><%=sprzet.getNr_ID()%></div>
				<div class="col-lg-2" style="height: 85px;"><%=sprzet.getNazwa()%></div>
				<div class="col-lg-1" style="height: 85px;"><%=sprzet.getProducent()%></div>
				<div class="col-lg-2" style="height: 85px;">
					<%
						for (Oddzial oddzial : branch) {
									if (sprzet.getID_oddzialu() == (oddzial.getID_oddzialu())) {
					%><%=oddzial.getNazwa()%>

					<%
						}
								}
					%>
				</div>
				<div class="col-lg-2" style="height: 85px;">
					<form class="label">
						<input type="hidden" name="numerID" value="<%=sprzet.getNr_ID()%>" />
						<button class="btn btn-primary btn-sm" type="submit" name="action"
							value="moreInfo">
							<i class="fa fa-info"></i>&nbsp; Wi�cej informacji
						</button>
					</form>

				</div>
				<div class="col-lg-2"
					style="height: 85px; background: none; border: none;"></div>
				<%
					}
				%>

			</div>
		</div>
		<%
			}
		%>

	</div>


</body>
</html>

