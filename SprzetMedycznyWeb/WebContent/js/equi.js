﻿$(document).ready(function() {
	$("#menu > li > div").click(function() {

		if (false == $(this).next().is(':visible')) {
			$('#menu ul').slideUp(300);
		}
		$(this).next().slideToggle(300);
	});
	$('#menu ul:eq(0)').show();

});

$(document).ready(function() {
	$("#grid tr:even").addClass('classEven');
});

$(document)
		.ready(
				function() {
					$("span.tooltip_message")
							.hover(
									function() {
										$(this)
												.append(
														'<div class="message"><p>Wyszukaj za pomocą słowa kluczowego:<ul><li>Numer ID</li><li>Nazwa</li><li>Producent</li><li>Nazwa oddziału</li></ul></p></div>');
									}, function() {
										$("div.message").remove();
									});
					$("span.tooltip_img1")
							.hover(
									function() {
										$(this)
												.append(
														'<div class="message"><ul><li>Tytuł - Beginning Groovy, Grails and Griffon</li><li>Autor: Vishal Layka</li><li>Wydawca: Apress</li></ul></div>');
									}, function() {
										$("div.message").remove();
									});
				});

$(document).ready(
		function() {

			$('#imie').on(
					'blur',
					function() {
						var input = $(this);
						var pattern = /^[a-zA-Z]{2,15}$/i;
						var is_name = pattern.test(input.val());
						if (is_name) {
							input.removeClass("invalid");
							input.next('.komunikat')
							s.removeClass("blad");
						} else {
							input.addClass("invalid");
							input.next('.komunikat').text(
									"Imię nie może zawierać cyfr")

							.addClass("blad");

						}
					});
			$('#nazwisko').on(
					'blur',
					function() {
						var input = $(this);
						var pattern = /^[a-zA-Z]{2,15}$/i;
						var is_name = pattern.test(input.val());
						if (is_name) {
							input.removeClass("invalid");
							input.next('.komunikat')
							s.removeClass("blad");
						} else {
							input.addClass("invalid");
							input.next('.komunikat').text(
									"Nazwisko nie może zawierać cyfr")

							.addClass("blad");

						}
					});
			$('#telefon').on(
					'blur',
					function() {
						var input = $(this);
						var name_length = input.val().length;
						if (name_length != 9) {
							input.removeClass("valid").addClass("invalid");
							input.next('.komunikat').text(
									"Telefon musi mieć 9 znaków!").removeClass(
									"ok").addClass("blad");
						} else {
							input.removeClass("invalid");
							input.next('.komunikat').removeClass("blad");
						}
					});
			$('#pesel').on(
					'blur',
					function() {
						var input = $(this);
						var name_length = input.val().length;
						if (name_length != 11) {
							input.removeClass("valid").addClass("invalid");
							input.next('.komunikat').text(
									"PESEL musi mieć 11 znaków!").removeClass(
									"ok").addClass("blad");
						} else {
							input.removeClass("invalid");
							input.next('.komunikat').removeClass("blad");
						}
					});

		});

$(document).ready(
		function() {

			// the min chars for username
			var min_chars = 3;

			// result texts
			var characters_error = 'Minimum amount of chars is 3';
			var checking_html = 'Checking...';

			// when button is clicked
			$('#login1').on(
					'blur',
					function() {
						var input = $(this);
						// run the character number check
						if ($('#login').val().length < min_chars) {
							// if it's bellow the minimum show characters_error
							// text '
							input.addClass("invalid");
							input.next('.komunikat').text(
									"Login musi mieć minimum 3 znaki!")
									.removeClass("ok").addClass("blad");
						} else {
							// else show the cheking_text and run the function
							// to check
							input.removeClass("invalid");
							input.removeClass("blad");
							check_availability();
						}
					});

		});

// function to check username availability
function check_availability() {

	// get the username
	var login = $('#login1').val();
	var input = $(this);

	// use ajax to run the check
	$.post("check_username.php", {
		login : login
	}, function(result) {
		input.next('.komunikat').text("laduje");
		// if the result is 1
		if (result == 1) {
			// show that the username is available
			input.removeClass("invalid");
			input.next('.komunikat').text("Login jest dostępny").removeClass(
					"blad");
		} else {
			// show that the username is NOT
			input.addClass("invalid");
			input.next('.komunikat').text("Login jest zajęty!").removeClass(
					"ok").addClass("blad");
		}
	});

}

$(document).ready(
		function() {
			$('#login').on(
					'blur',
					function()
				    {
						var name=document.getElementById( "login" ).value;
						
						   if(name)
						   {
						        $.ajax({
								   type: 'post',
								   url: 'check_username.php',
								   data: {
								   user_name:name,
								   },
								   success: function (response) {
								   $( '#name_status' ).html(response);
					  		          if(response=="OK")	
					                  {
					                     return true;	
					                  }
					                  else
					                  {
					                     return false;	
					                  }
					                }
							      });
						
						   }
						   else
						   {
							   $( '#name_status' ).html("");
							   return false;
						   }
						})
		});
