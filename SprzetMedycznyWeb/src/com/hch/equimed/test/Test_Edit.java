/**
 * 
 */
package com.hch.equimed.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.hch.equimed.dao.EquimedDAOImpl;
import com.hch.equimed.dao.UserDAOImpl;
import com.hch.equimed.dao.DBMySQL;
import com.hch.equimed.dao.DBOracle;
import com.hch.equimed.dao.EditDAOImpl;
import com.hch.equimed.model.Sprzet;
import com.hch.equimed.model.Uzytkownik;

/**
 * @author Magic
 *
 */
public class Test_Edit extends EditDAOImpl {
	UserDAOImpl userDao = new UserDAOImpl();
	EquimedDAOImpl equipDao = new EquimedDAOImpl();

	String login = "test_login";
	String imie = "test_imie";
	String nazwisko = "test_nazwisko";
	String telefon = "123456";
	String email = "test_email";
	String pesel = "145236";
	String prawoDostepu = "1";

	int id_oddzialu = 101;
	int nr_id = 600;
	int nr_in = 600;
	int newNr_in = 601;
	String data = "2016-04-15";
	String nazwa = "EKG 15";
	String producent = "SECA";
	String data_zakupu = "2016-01-03";
	String dw = "2016-01-05";
	String gwarancja = "2018-01-03";
	String ostatni_przeglad = "2016-01-04";
	String ostatni_przeglad_upd = "2018-01-04";

	/**
	 * Test method for
	 * {@link com.hch.equimed.dao.EditDAOImpl#updateUser(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int)}
	 * .
	 */
	@Test
	public void testUpdateUser() {
		userDao.addUser(login, imie, nazwisko, telefon, email, id_oddzialu, pesel, prawoDostepu);
		updateUser("test_login", "new haslo", "test_change", "654321", "test_change", 105);
		List<Uzytkownik> uzytkownicy = userDao.findAllUser();

		for (Uzytkownik uzytkownik : uzytkownicy) {
			if (uzytkownik.getLogin().equals(login)) {
				assertTrue(uzytkownik.getNazwisko().equals("test_change"));

				assertTrue(uzytkownik.getTelefon().equals("654321"));

				assertTrue(uzytkownik.getEmail().equals("test_change"));

				assertTrue(uzytkownik.getID_oddzialu_FK() == 105);
			}
		}
	}

	/**
	 * Test method for
	 * {@link com.hch.equimed.dao.EditDAOImpl#changeStatus(int, int, int)}.
	 */
	@Test
	public void testChangeStatus() {
		assertTrue(equipDao.addEquip(nr_id, nazwa, producent, nr_in, data_zakupu, dw, gwarancja, ostatni_przeglad,
				id_oddzialu));
		assertFalse(equipDao.addEquip(nr_id, nazwa, producent, nr_in, data_zakupu, dw, gwarancja, ostatni_przeglad,
				id_oddzialu));
		List<Sprzet> sprzety = equipDao.findAllEquipment();
		for (Sprzet sprzet : sprzety) {
			if (sprzet.getID_oddzialu() == nr_id) {
				assertTrue(sprzet.getAwaria() == 0);
				assertTrue(sprzet.getWypozyczenie() == 0);
			}
			changeStatus(nr_id, 1, 1);
			if (sprzet.getID_oddzialu() == nr_id) {
				assertTrue(sprzet.getAwaria() == 1);
				assertTrue(sprzet.getWypozyczenie() == 1);
			}
		}

	}

	/**
	 * Test method for
	 * {@link com.hch.equimed.dao.EditDAOImpl#changeNrIn(int, int, int)}.
	 */
	@Test
	public void testChangeNrIn() {
		List<Sprzet> sprzety = equipDao.findAllEquipment();
		for (Sprzet sprzet : sprzety) {
			if (sprzet.getID_oddzialu() == nr_id) {
				assertTrue(sprzet.getNr_in() == nr_id);
			}
			changeNrIn(nr_id, newNr_in);
			if (sprzet.getID_oddzialu() == nr_id) {
				assertTrue(sprzet.getNr_in() == newNr_in);
				assertFalse(sprzet.getNr_in() == nr_id);
			}
		}
	}

	/**
	 * Test method for
	 * {@link com.hch.equimed.dao.EditDAOImpl#changeDate(int, int, int)}.
	 */
	@Test
	public void testChangeDate() {
		List<Sprzet> sprzety = equipDao.findAllEquipment();
		for (Sprzet sprzet : sprzety) {
			if (sprzet.getID_oddzialu() == nr_id) {
				assertTrue(sprzet.getOstatniPrzeglad().equals(ostatni_przeglad));
			}
			changeDate(nr_id, ostatni_przeglad_upd);
			if (sprzet.getID_oddzialu() == 502) {
				assertFalse(sprzet.getOstatniPrzeglad().equals(ostatni_przeglad));
				assertTrue(sprzet.getOstatniPrzeglad().equals(ostatni_przeglad_upd));
			}
		}
		String sql = "delete from sprzet where nr_id='" + nr_id + "'";
		DBOracle.connectOracle(sql);
	}

}
